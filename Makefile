#
# clickgraph.jar makefile
#    Use: 	cd extension/clickgraph/
#			make
#
# When make runs, it looks like only three files are compiled by javac, but all the classes are generated.
# Not sure what this is about.
# C. Turner
# 4-23-12
#

# General section
TARGET 		= clickgraph.jar
ZIPTARGET	= clickgraph
CLASSPATH 	= ../../NetLogo_5.0.5/NetLogo.jar:commons-jexl-2.1.1.jar:commons-logging-1.1.1.jar:imgscalr-lib-4.2.jar
SOURCEPATH 	= src:.


CLASS_DIR 	= classes
SRC_DIR 	= src

# Command section
JAVA		= javac
JAR			= jar
MAKE		= make
RM			= rm
RMF			= rm -rf

SOURCES = $(wildcard src/*.java)
CLASSES = $(patsubst src/%.java,classes/%.class,$(SOURCES))

# General rules
$(TARGET): $(CLASSES)
	$(JAR) cvfm $(TARGET) manifest.txt -C $(CLASS_DIR) .
	$(MAKE) zipfile

clean:
	$(RM) $(CLASS_DIR)/*
	$(RM) $(TARGET)
	$(RMF) $(ZIPTARGET)
	$(RM) $(ZIPTARGET).zip

print_vars:
	echo "These are the source files:"
	echo $(SOURCES)
	echo "These are the generated class files:"
	echo $(CLASSES)

zipfile:
	$(RMF) $(ZIPTARGET)
	mkdir $(ZIPTARGET)
	cp Makefile $(ZIPTARGET)/
	cp manifest.txt $(ZIPTARGET)/
	cp README.md $(ZIPTARGET)/
	cp *.jar $(ZIPTARGET)/
	cp *.nlogo $(ZIPTARGET)/
	zip -r $(ZIPTARGET) $(ZIPTARGET)


# File rules
classes/%.class: src/%.java
	$(JAVA) -sourcepath $(SOURCEPATH) -classpath $(CLASSPATH) -d $(CLASS_DIR) $<
