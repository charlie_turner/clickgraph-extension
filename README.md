# clickgraph-extension

## ClickGraph Extension for NetLogo 5.0.x

This NetLogo extension provides a small multiples graphing capability layered ontop of NetLogo's graphing area.

It uses the following libraries: 

*	[Java Expression Language (JEXL) version 2.1.1)](http://commons.apache.org/proper/commons-jexl)
*	[Java Logging Component (version 1.1.1)](http://commons.apache.org/proper/commons-logging) 
*	[imgscalr – Java Image Scaling Library (version 4.2)](http://www.thebuzzmedia.com/software/imgscalr-java-image-scaling-library) 

## CONTENTS:
In addition to the source, which expects to be compiled against a 5.0.x version of NetLogo and Makefile, this repo contains a ZIP file that can be unzipped and used without compilation.

The ZIP file contains a compiled version of clickgraph, all required libraries, as well as a sample NetLogo model that can be used to demonstrate the clickgraph commands.  

## TO USE:
For a first use without compiling code, do the following:

1.	Acquire the NetLogo software (5.0.5 is the latest version as of this writing).
2.	Clone this repository onto your machine.
3.	Place the ZIP file (clickgraph.zip) from this repo into the extensions subfolder of your NetLogo installation
4.	unzip, resulting in a folder called "clickgraph" under extensions.

If you would like to use the source code for this extension, the following installation steps are required.

1. Acquire and install the NetLogo software (5.0.5 is the latest version as of this writing).
2. Acquire each of the jar files mentioned above (they are also in the zip file)
3. clone this project into the **NetLogo/extensions/clickgraph** directory
4. cd clickgraph
5. Edit Makefile, updating **../../NetLogo_5.0.5/NetLogo.jar** in CLASSPATH so that it is correct for your installation.  
6. make (generating clickgraph.jar)

## NOTES:

To run the test program for the library, do the following:

1.	Perform all the steps in **TO USE** section.
2.	Copy testModelClickgraph-ExtensionNL505.nlogo from the clickgraph folder into the NetLogo model folder.
3.	Start NetLogo, browse to File->Models Library, and select testModelClickgraph-ExtensionNL505.
4.	Use the buttons to exercise different functionality of clickgraph.

extensions [clickgraph]

*TODO: add clickgraph commands here*

## QUESTIONS:
If you run into problems or have questions about the extension, please email me: cjturner@ucdavis.edu
I have tested early versions of this code on Mac only. 


