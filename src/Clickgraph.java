/********************************************************************************
 * Clickgraph.java: Top-level primitive file for NetLogo clickgraph extension.
 *		Commands in this file operate above the workspace level.  It contains
 *		information about the NetLogo World configuration.
 *		
 * See Workspace.java for the next level down in the object hieararchy.
 *******************************************************************************/

//package org.nlogo.extensions.clickgraph;

import org.nlogo.api.LogoException;
import org.nlogo.api.ExtensionException;
import org.nlogo.api.Argument;
import org.nlogo.api.Syntax;
import org.nlogo.api.Context;
import org.nlogo.api.DefaultReporter;
import org.nlogo.api.DefaultCommand;

/* includes needed for WriteToNetLogo - problems with Name clash on Workspace
import org.nlogo.nvm.ExtensionContext;
import org.nlogo.nvm.Workspace;
*/


// graphing support
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.BasicStroke;

import java.util.ArrayList;
import java.awt.Color;
import java.util.Random; 

public class Clickgraph {
	
	static private int min_pxcor, max_pxcor;	// absolute bounds of NetLogo's graph window in the X [horizontal] dimension
	static private int min_pycor, max_pycor;	// absolute bounds of NetLogo's graph window in the Y [vertical] dimension
	static private double patch_size; 			// NetLogo's patch-size (any positive real number)

	static Color fill_color = new Color(87, 87, 87);   // background of graph window - somewhere between lightGray and darkGray;

	
	static boolean draw_meshp = false;	// boolean controlling whether to draw graph grid
	// various colors for different graph attributes
	static java.awt.Color text_color = java.awt.Color.white;
	static java.awt.Color text_error_color = java.awt.Color.magenta;
	static java.awt.Color stroke_color = java.awt.Color.white;
	static java.awt.Color axes_color = java.awt.Color.white;
	static java.awt.Color mesh_color = java.awt.Color.darkGray;
	static java.awt.Color green_line_color = java.awt.Color.green;
	static java.awt.Color yellow_line_color = java.awt.Color.yellow;

	static Stroke stroke = new BasicStroke(1);
	static ArrayList<ColorMapItem> ColorMap = new ArrayList<ColorMapItem>();
	
	/********************************************************************************
	 *  NETLOGO Extension PRIMITIVES 
	 *******************************************************************************/
	
	/********************************************************************************
	 * setup: NetLogo primitive to perform most basic initialization of
	 *		the clickgraph extension.
	 *******************************************************************************/
	
	public static class Setup extends DefaultCommand {
		
		public Syntax getSyntax() {
			int[] right = { Syntax.NumberType(), 	// min-pxcor
							Syntax.NumberType(), 	// max-pxcor
							Syntax.NumberType(),	// min-pycor
							Syntax.NumberType(), 	// max-pycor
							Syntax.NumberType()};	// patch-size
			return Syntax.commandSyntax(right);
		}
	
		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			
			// Extract arguments
			try {
				Clickgraph.min_pxcor 	= args[0].getIntValue();
				Clickgraph.max_pxcor 	= args[1].getIntValue();
				Clickgraph.min_pycor 	= args[2].getIntValue();
				Clickgraph.max_pycor 	= args[3].getIntValue();
				Clickgraph.patch_size	= args[4].getDoubleValue();
			} 
			
			// Hope that they have the correct type
			catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() ) ;
			}
			
			Workspace.initWorkspaces();	// clear out any existing workspaces (and their subordinate graphs)
			
			debug("initialize: pxcor: [" + Clickgraph.min_pxcor + "," + Clickgraph.max_pxcor + "] pycor: [" + Clickgraph.min_pycor + "," + Clickgraph.max_pycor + "] patch-size: " + Clickgraph.patch_size);
		
			initColorMap();				// map convenient color names to java Color objects
		}
	}
	
	public static class Regraph extends DefaultCommand  
	{	
		public Syntax getSyntax() {
			// x, y, width, height
			int[] right = { };
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			Workspace.Regraph(context);
		}
	}
	

	/********************************************************************************
	 *  INSTANCE methods
	 *******************************************************************************/

	/********************************************************************************
	 *  CLASS methods
	 *******************************************************************************/

	// define getters for world bounds - commonly used in workspace drawing
	public static int get_min_pxcor() {
		return Clickgraph.min_pxcor;
	}
	public static int get_max_pxcor() {
		return Clickgraph.max_pxcor;
	}
	public static int get_min_pycor() {
		return Clickgraph.min_pycor;
	}
	public static int get_max_pycor() {
		return Clickgraph.max_pycor;
	}
	public static double get_patch_size() {
		return Clickgraph.patch_size;
	}
	
	public static int get_world_width() {
		return Clickgraph.max_pxcor - Clickgraph.min_pxcor + 1;
	}
	public static int get_world_height() {
		return Clickgraph.max_pycor - Clickgraph.min_pycor + 1;
	}


	// eraseWorld: this is done by drawing a rectangle into the entire workspace area
	//		as defined by min_pxcor <-> max_pxcor and min_pycor <-> max_pycor
	//		the rectangle drawn is defined by the class variable fill_color above
	//		context is passed in from the NetLogo primitive that calls this function
	
	public static void eraseWorld(Context context) {
		// get the drawing area from the Netlogo Context
		BufferedImage drawing = context.getDrawing();
		Graphics2D gc = drawing.createGraphics();		// graphics context
		
		gc.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
		gc.setColor( Clickgraph.fill_color ); 
		
		// get world dimensions 
		double window_width = Clickgraph.max_pxcor - Clickgraph.min_pxcor + 1;		// +1 to get right edge
		double window_height = Clickgraph.max_pycor - Clickgraph.min_pycor + 1;	// +1 to get bottom edge
		
		// draw the rectangle, scaling upper left and world_size by patch_size
	
		gc.fillRect(0, 0,
			(int) (window_width * Clickgraph.patch_size),
			(int) (window_height * Clickgraph.patch_size));
	}
	
	
	// convert student coordinates (in the range -dim to dim e.g. -10 .. 10) to 
	// the appropriate Netlogo world location.  The +/- 0.5 are for offsets to 
	// the center of the markers/turtles
	// needs to return double because patch_size is double
	
	public static double convertXCoord(double cx) {
		//return (int) ((cx - Clickgraph.min_pxcor + 0.5) * Clickgraph.patch_size);
		return (cx - (double) Clickgraph.min_pxcor + 0.5) * Clickgraph.patch_size;
	}
	
	public static double convertYCoord(double cy) {
		//return (int) (- (cy - Clickgraph.max_pycor - 0.5) * Clickgraph.patch_size);
		return (- (cy - (double) Clickgraph.max_pycor - 0.5) * Clickgraph.patch_size);
	}
	
	
	public static void debug(String str) {
		System.out.println(str);
	}
	
	static Random rand = new Random(); 
	

	static class ColorMapItem {
		String name;
		int red, green, blue;
		
		public ColorMapItem(String name, int red, int green, int blue) {
			this.name = new String(name);
			this.red = red;
			this.green = green;
			this.blue = blue;
		}
	}
	
	static public void initColorMap() {
		ColorMap.clear();
		ColorMap.add(new ColorMapItem("white",			255,	255,	255		));
		ColorMap.add(new ColorMapItem("black",			0,		0,		0		));
		ColorMap.add(new ColorMapItem("bggray",			87,		87,		87		));		// netlogo's black + 3
		ColorMap.add(new ColorMapItem("gray",			128,	128,	128		));
		ColorMap.add(new ColorMapItem("lightgray",		211,	211,	211		));
		ColorMap.add(new ColorMapItem("red",			255,	0,		0		));
		ColorMap.add(new ColorMapItem("orange",			255,	165,	0		));
		ColorMap.add(new ColorMapItem("brown",			165,	42,		42		));
		ColorMap.add(new ColorMapItem("yellow",			255,	255,	0		));
		ColorMap.add(new ColorMapItem("green",			0,		255,	0		));
		ColorMap.add(new ColorMapItem("lime",			50,		205,	50		));
		ColorMap.add(new ColorMapItem("turquoise",		64,		224,	208		));
		ColorMap.add(new ColorMapItem("cyan",			0,		255,	255		));
		ColorMap.add(new ColorMapItem("sky",			135,	206,	235		));
		ColorMap.add(new ColorMapItem("blue",			0,		0,		255		));
		ColorMap.add(new ColorMapItem("violet",			238,	130,	238		));
		ColorMap.add(new ColorMapItem("magenta",		255,	0,		255		));
		ColorMap.add(new ColorMapItem("pink",			255,	0,		0		));		// was 255,192,203 - but this didn't match old navgraph
	}
	
	static public void dumpColorMap() {
		debug("Dump ColorMap: ");
		
		for (ColorMapItem cmi: ColorMap ) {
			debug("Color: " + cmi.name + " " + cmi.red + " " + cmi.green + " " + cmi.blue);
		}		
	}
	
	
	static public Color findColor(String cname) {
		String lcname = cname.toLowerCase();
		Color myColor;
		//NavgraphExtension.debug("findColor " + cname);
		
		if (cname.equals("random")) {
			int idx = rand.nextInt(ColorMap.size());
			ColorMapItem cmi = ColorMap.get(idx);
			myColor = new Color (cmi.red, cmi.green, cmi.blue);
			return myColor;
		} else {
			for (ColorMapItem cmi: ColorMap ) {
				if (lcname.equals(cmi.name)) {
					//NavgraphExtension.debug("Found Color: " + cmi.name + " " + cmi.red + " " + cmi.green + " " + cmi.blue);
					myColor = new Color (cmi.red, cmi.green, cmi.blue);
					return  myColor;
				}
			}	
			debug("findColor failed to find color " + cname);
			return null;
		}
	}	
	
	

}
