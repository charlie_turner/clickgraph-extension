
/*
 * This is the main class for the clickgraph extension.  
 * It defines the various commands and reporters.
 */

//package org.nlogo.extensions.clickgraph;

import org.nlogo.api.DefaultClassManager;
import org.nlogo.api.PrimitiveManager;

public class ClickgraphExtension extends DefaultClassManager {
	
	/***********************************************************************
	 * Define the extension primitives.
	 ***********************************************************************/
	
	public void load(PrimitiveManager primitiveManager) {
		
		// clickgraph system level primitives
		primitiveManager.addPrimitive("setup", new Clickgraph.Setup());
		primitiveManager.addPrimitive("regraph", new Clickgraph.Regraph());
		
		// probably not needed by users
		primitiveManager.addPrimitive("dump-workspaces", new Workspace.DumpWorkspaces()); 
		primitiveManager.addPrimitive("clear-workspaces", new Workspace.ClearWorkspaces());
		
		// workspace primitives
		primitiveManager.addPrimitive("workspace-create", new Workspace.Create());
		primitiveManager.addPrimitive("workspace-destroy", new Workspace.Destroy());
		primitiveManager.addPrimitive("workspace-show", new Workspace.Show());
		primitiveManager.addPrimitive("workspace-hide", new Workspace.Hide());
		primitiveManager.addPrimitive("workspace-set-margin", new Workspace.SetMargin());
		primitiveManager.addPrimitive("workspace-set-padding", new Workspace.SetPadding());
		primitiveManager.addPrimitive("workspace-show-line-equations", new Workspace.ShowLineEquations());	// 11-26-14
		primitiveManager.addPrimitive("workspace-show-lines", new Workspace.ShowLines());

		primitiveManager.addPrimitive("workspace-add-grapher", new Workspace.AddGrapher()); 
		primitiveManager.addPrimitive("workspace-get-grapher-names", new Workspace.GetGrapherNames());
		primitiveManager.addPrimitive("workspace-get-num-graphers", new Workspace.GetNumGraphers());
		
		
		// grapher primitives - there are 1 more graphers per workspace
		primitiveManager.addPrimitive("grapher-show", new Grapher.Show());
		primitiveManager.addPrimitive("grapher-hide", new Grapher.Hide());
		primitiveManager.addPrimitive("grapher-get-specs", new Grapher.GetSpecs());
		primitiveManager.addPrimitive("grapher-inboundsp", new Grapher.InBoundsP());
		primitiveManager.addPrimitive("grapher-to-patch-coord", new Grapher.ToPatchCoord());
		
		primitiveManager.addPrimitive("grapher-set-location", new Grapher.SetLocation());
		primitiveManager.addPrimitive("grapher-set-background", new Grapher.SetBackground());
		
		primitiveManager.addPrimitive("grapher-set-window-range", new Grapher.SetWindowRange());
		primitiveManager.addPrimitive("grapher-set-size", new Grapher.SetSize());
		primitiveManager.addPrimitive("grapher-set-zoom", new Grapher.SetZoom());
		
		primitiveManager.addPrimitive("grapher-show-axes", new Grapher.ShowAxes());
		primitiveManager.addPrimitive("grapher-label-axes", new Grapher.LabelAxes());
		
		primitiveManager.addPrimitive("grapher-set-axes-color", new Grapher.SetAxesColor());
		primitiveManager.addPrimitive("grapher-set-axes-weight", new Grapher.SetAxesWeight());
		
		primitiveManager.addPrimitive("grapher-draw-point", new Grapher.DrawPoint());
		primitiveManager.addPrimitive("grapher-draw-image", new Grapher.DrawImage());
		primitiveManager.addPrimitive("grapher-clear-images", new Grapher.ClearImages());
		primitiveManager.addPrimitive("grapher-copy-image", new Grapher.CopyImage());

		primitiveManager.addPrimitive("grapher-set-group-equation", new Grapher.SetGroupEquation());
		primitiveManager.addPrimitive("grapher-dump-equations", new Grapher.DumpEquations());
		
		// equation primitives - there are 1 or more equations per grapher
		primitiveManager.addPrimitive("equation-add-custom", new Equation.AddCustom());
		primitiveManager.addPrimitive("equation-delete", new Equation.Delete());
		primitiveManager.addPrimitive("equation-delete-all", new Equation.DeleteAll());
		primitiveManager.addPrimitive("equation-get-value", new Equation.GetValue());
		
	}
}


