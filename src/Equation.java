/********************************************************************************
 * Equation.java: 3nd-level file for NetLogo clickgraph extension.
 *	worspaces own graphers, which own equations
 *******************************************************************************/

// NetLogo extension API includes
import org.nlogo.api.LogoException;
import org.nlogo.api.ExtensionException;
import org.nlogo.api.Argument;
import org.nlogo.api.Syntax;
import org.nlogo.api.Context;
import org.nlogo.api.DefaultReporter;
import org.nlogo.api.DefaultCommand;
import org.nlogo.api.LogoListBuilder;
import org.nlogo.api.LogoList;

import java.awt.Graphics2D;
import java.awt.geom.Line2D;	// for intersection
import java.awt.geom.Point2D;


//import java.awt.RenderingHints;
//import java.awt.geom.Rectangle2D;
//import java.awt.geom.Ellipse2D;

import java.awt.Stroke;
import java.awt.BasicStroke;
//import java.awt.Font;

import java.util.LinkedList;

import org.apache.commons.jexl2.*;	// for expression evalation

public class Equation {            
	String name;
	String expression;
	String color;
	String type;
	Boolean visiblep;		// controls when teacher toggles display of lines 
	Boolean activep;		// initially set to true, gets set to false when equation is deleted
	
	static public final int THICK_STROKE = 3;
	static public final int THIN_STROKE = 1;
	
	// class variable for tracking all created equation instances
	static LinkedList<Equation> EquationList = new LinkedList<Equation>();
	
	// instance and class methods are below NetLogo primitives.
	
	/********************************************************************************
	 *  NETLOGO Extension PRIMITIVES 
	 *******************************************************************************/
	
	
	/********************************************************************************
	 * equation-add-custom: NetLogo primitive to add an equation to a grapher
	 *	inputs: grapher_name, equation_name, expression, color, type
	 *	it is an error to try to update a grapher that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/

	public static class AddCustom extends DefaultCommand {
		
		public Syntax getSyntax() {
			int[] right = {Syntax.StringType(), Syntax.StringType(), Syntax.StringType(), Syntax.StringType(), Syntax.StringType()};
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String name;
		 	String eq_name;
			String expression;
			String color;
			String type;
			Grapher g;
			Equation this_eq = null;

			try {
				name 		= args[0].getString();		// grapher name
				eq_name 	= args[1].getString();
				expression 	= args[2].getString();
				color 		= args[3].getString();
				type 		= args[4].getString();
				
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}
			
			if ((g = Grapher.findGrapherByName(name)) == null) {
				throw new ExtensionException( "equation-add-custom: unable to find grapher named '" + name + "'" );
			}
			

			for (Equation eq: g.equations) {
				if (eq_name.equals(eq.name)) {
					this_eq = eq;
				}
			}

			if (this_eq != null) {
				if (!expression.equals(this_eq.expression) || !color.equals(this_eq.color) || !type.equals(this_eq.type)) {
					this_eq.expression = new String(expression);
					this_eq.color = new String(color);
					this_eq.type = new String(type);
				}
				this_eq.activep = true;
			} else {
				Equation eq = new Equation(eq_name, expression, color, type);
				g.equations.add(eq);
			}

			g.redraw(context);

		}		
	}
	
	
	/********************************************************************************
	 * equation-delete: NetLogo primitive to remove one equation from this grapher
	 *	inputs: grapher_name equation_name
	 * 		this function will also remove the equation from global equation list
	 *	it is an error to try to update a grapher that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/
	
	
	public static class Delete extends DefaultCommand  
	{
		
		public Syntax getSyntax() {
			// x, y, width, height
			int[] right = { Syntax.StringType(), Syntax.StringType()};
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String name;
		 	String eq_name;
			int someothernumber;
			Grapher g;
			try {

				name = args[0].getString();
		 		eq_name = args[1].getString();

			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}
			
			if ((g = Grapher.findGrapherByName(name)) == null) {
				throw new ExtensionException( "equation-delete: unable to find grapher named '" + name + "'" );
			}

			for (Equation eq: g.equations) {
				if (eq_name.equals(eq.name)) {
					eq.activep = false;
				}
			}
		}
	}
	
	/********************************************************************************
	 * equation-delete-all: NetLogo primitive to remove all equations from this grapher
	 *	inputs: grapher_name
	 * 		this function will also remove equations from global equation list
	 *	it is an error to try to update a grapher that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/


	public static class DeleteAll extends DefaultCommand  
	{
		public Syntax getSyntax() {
			// x, y, width, height
			int[] right = {Syntax.StringType()};
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String name;
			Grapher g;

			try {
				name 		= args[0].getString();		// grapher name				
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}

			if ((g = Grapher.findGrapherByName(name)) == null) {
				throw new ExtensionException( "equation-delete-all: unable to find grapher named '" + name + "'" );
			}

			// remove all of this graphers from global equation list

			for (Equation eq: g.equations) {
				Equation.EquationList.remove(eq);
			}
			// now clear this grapher's equations
			g.equations.clear();

		}
	}
	
	
	/********************************************************************************
	 * equation-show: NetLogo primitive to add an equation to a grapher
	 *	inputs: grapher_name, equation_name, expression, color, type
	 *	it is an error to try to update a grapher that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/

	public static class Show extends DefaultCommand {
		
		public Syntax getSyntax() {
			int[] right = {Syntax.StringType(), Syntax.StringType()};
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String name;
		 	String eq_name;
			Boolean foundp = false;
			Grapher g;

			try {
				name 		= args[0].getString();		// grapher name
				eq_name 	= args[1].getString();				
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}

			if ((g = Grapher.findGrapherByName(name)) == null) {
				throw new ExtensionException( "equation-show: unable to find grapher named '" + name + "'" );
			}
			
			for (Equation eq: g.equations) {
				if (eq_name.equals(eq.name)) {
					g.redraw(context);
					foundp = true;
					eq.visiblep = true;
				}
			}
			
			if (!foundp) {
				throw new ExtensionException( "equation-show: unable to find equation named '" + eq_name + "' in grapher " + name );
		 	}
		}		
	}

	
	/********************************************************************************
	 * equation-hide: NetLogo primitive to add an equation to a grapher
	 *	inputs: grapher_name, equation_name, expression, color, type
	 *	it is an error to try to update a grapher that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/

	public static class Hide extends DefaultCommand {
		
		public Syntax getSyntax() {
			int[] right = {Syntax.StringType(), Syntax.StringType()};
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String name;
		 	String eq_name;
			Boolean foundp = false;
			Grapher g;

			try {
				name 		= args[0].getString();		// grapher name
				eq_name 	= args[1].getString();				
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}

			if ((g = Grapher.findGrapherByName(name)) == null) {
				throw new ExtensionException( "equation-hide: unable to find grapher named '" + name + "'" );
			}
			
			for (Equation eq: g.equations) {
				if (eq_name.equals(eq.name)) {
					eq.visiblep = false;
					g.redraw(context);
					foundp = true;
				}
			}
			
			if (!foundp) {
				throw new ExtensionException( "equation-hide: unable to find equation named '" + eq_name + "' in grapher " + name );
		 	}
		}		
	}

	
	/********************************************************************************
	 * equation-get-value: NetLogo primitive to get xxxx
// TODO: Equation.equation-get-value: add accurate comments to GetValue
	 *	inputs: grapher_name, equation_name, value1, value2
	 *	it is an error to try to update a grapher that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/

	public static class GetValue extends DefaultReporter {
	
	 // take one number as input, report a list
	  public Syntax getSyntax() {
	    return Syntax.reporterSyntax(
	    new int[] {Syntax.StringType(), Syntax.StringType(), Syntax.NumberType(), Syntax.NumberType() }, Syntax.ListType());
	  }

	  public Object report(Argument args[], Context context)
	      throws ExtensionException {
	    // create a NetLogo list for the result
	    LogoListBuilder list = new LogoListBuilder();
		String name, eq_name;
		int someothernumber;
	    int x;
	    // use typesafe helper method from 
	    // org.nlogo.api.Argument to access argument
	    try {
			name 			=  args[0].getString();  
			eq_name 		=  args[1].getString();  
			someothernumber = args[2].getIntValue();  
	      	x 				= args[3].getIntValue();  
	    }
	    catch(LogoException e) {
	      throw new ExtensionException( e.getMessage() ) ;
	    }
	
		throw new ExtensionException( "equation-get-value: No idea what parameters 3 & 4 do: " + name + " eq_name = " + eq_name + " number= " +someothernumber + " x = " + x);
	
// TODO: Equation.GetValue: unreachable code commented out after manditory exception
	    // populate the list. note that we use Double objects; NetLogo
	    // numbers are always Doubles
	    //for (int i = 0; i < 2; i++) {
	    //  list.add(Double.valueOf(i));
	    //}
	    //return list.toLogoList();
	  }
	}
	
	
	
	/********************************************************************************
	 *   INSTANCE Methods 
	 *******************************************************************************/

	public void dump() {
		Clickgraph.debug("--- Equation: " + this.name);
		Clickgraph.debug("    expression: " + this.expression);
		Clickgraph.debug("    color: " + this.color);
		Clickgraph.debug("    type: " + this.type);
		Clickgraph.debug("    visiblep: " + this.visiblep);
		Clickgraph.debug("    activep: " + this.activep);
	}
	
	
	


	public void draw(Grapher g, Graphics2D g3, boolean vertical_shading) throws Exception{
		double x1, y1, x2, y2;
		double fx1, fy1, fx2, fy2;		// fill-x1,x2,y1,y2
		double dx, slope, ymin, ymax;			// used to control shading
		double x1s, y1s, x2s, y2s;	// shifted to origin
		double xscale, yscale; 				// for scaling x1, x2, y1, y2;
		java.awt.Color pen_color;
		Object obj;

		if ((this.expression != null) && ("".equals(this.expression) != true) && (this.visiblep == true) && (this.activep == true)) {

			x1 = g.xrange.gmin;
			x2 = g.xrange.gmax;
	        /**
	         * First step is to retrieve an instance of a JexlEngine;
	         * it might be already existing and shared or created anew.
	         */
	        JexlEngine jexl = new JexlEngine();
	        /*
	         *  Second make a jexlContext and put stuff in it
	         */

			String flt_expr = this.floatExpression();

		    Expression e = jexl.createExpression( flt_expr );  //this.expression );

		    // populate the context
		    JexlContext context = new MapContext();
		    context.set("x", x1);		// cover upper or lower case
			context.set("X", x1);

		    // work it out

			obj = e.evaluate(context);
			y1 = convertToDouble(obj);

		 	context.set("x", x2);		// cover upper or lower case
			context.set("X", x2);
			obj = e.evaluate(context);
			y2 = convertToDouble(obj);

			if (x1 == x2) {
				slope = 1.0;		// don't want divide by zero error
			} else {
				slope = (y2-y1) / (x2-x1);
			}
		 	xscale = g.xsize / (g.xrange.gmax - g.xrange.gmin);		// pixels per tick
			yscale = g.ysize / (g.yrange.gmax - g.yrange.gmin);
			dx = 1.0 / (xscale * 2.0); 	// for the grid lines - to get uniform density no matter what the dim

			x1s = (x1 * xscale) + g.xpos + (0.5 * (double) g.xsize);
			y1s = (y1 * yscale) + (double) g.ypos - (0.5 * (double) g.ysize);
			x2s = (x2 * xscale) + g.xpos + (0.5 * (double) g.xsize);
			y2s = (y2 * yscale) + (double) g.ypos - (0.5 * (double) g.ysize);

			pen_color = Clickgraph.findColor(this.color);
			if (pen_color != null) {
				g3.setColor( pen_color ); 
			}
			if ("thick".equals(this.type)) {
				Stroke stroke = new BasicStroke(THICK_STROKE);
				g3.setStroke( stroke );		
			} else if ("thin".equals(this.type)) {
				Stroke stroke = new BasicStroke(THIN_STROKE);
				g3.setStroke( stroke );
			}

			g.drawGraphLine(g3, x1s, y1s, x2s, y2s);

			// draw area lines
			if ("below".equals(this.type)) {
				Stroke stroke = new BasicStroke(THIN_STROKE);
				g3.setStroke( stroke );
				if (vertical_shading) {
					x1 = x2 = g.xrange.gmin;
					y1 = g.yrange.gmin;

					while (x1 < g.xrange.gmax) {
						context.set("x", x2);		// cover upper and lower case equations
						context.set("X", x2);
						// y2 = (Double)e.evaluate(context);
						obj = e.evaluate(context);
						y2 = convertToDouble(obj);

						x1s = (x1 * xscale) + g.xpos + (0.5 * (double) g.xsize);
						y1s = (y1 * yscale) + (double) g.ypos - (0.5 * (double) g.ysize);
						x2s = (x2 * xscale) + g.xpos + (0.5 * (double) g.xsize);
						y2s = (y2 * yscale) + (double) g.ypos - (0.5 * (double) g.ysize);
						g.drawGraphLine(g3, x1s, y1s, x2s, y2s);

						x1 += dx;
						x2 += dx;
					}
				} else {
					Line2D.Double eq_line = new Line2D.Double(x1, y1, x2, y2);		// used in fill region later
					Line2D.Double fill_line = new Line2D.Double();
					ymax = Math.max(y1, y2);		// where to stop the loop

					fx1 = g.xrange.gmin;
					fx2 = g.xrange.gmax;
					fy1 = g.yrange.gmin;

					while (fy1 <= ymax) {
						fy2 = fy1;
						fill_line.x1 = fx1;
						fill_line.y1 = fy1;
						fill_line.x2 = fx2;
						fill_line.y2 = fy2;

						Point2D.Double p = line_intersect(eq_line, fill_line);
						if (p != null) {
							// lines intersect
							if (slope >= 0) {
								fx1 = p.x;
								fy1 = p.y;
							} else {
								fx2 = p.x;
								fy2 = p.y;
							}
						}

						x1s = (fx1 * xscale) + g.xpos + (0.5 * (double) g.xsize);
						y1s = (fy1 * yscale) + (double) g.ypos - (0.5 * (double) g.ysize);
						x2s = (fx2 * xscale) + g.xpos + (0.5 * (double) g.xsize);
						y2s = (fy2 * yscale) + (double) g.ypos - (0.5 * (double) g.ysize);
						g.drawGraphLine(g3, x1s, y1s, x2s, y2s);

						fy1 += dx;
					}
				}
			} else if ("above".equals(this.type)) {
				Stroke stroke = new BasicStroke(THIN_STROKE);
				g3.setStroke( stroke );

				if (vertical_shading) {
					x1 = x2 = g.xrange.gmin;
					y1 = g.yrange.gmax;

					while (x1 < g.xrange.gmax) {
						context.set("x", x2);		// cover upper and lower case equations
						context.set("X", x2);
						// y2 = (Double)e.evaluate(context);
						obj = e.evaluate(context);
						y2 = convertToDouble(obj);

						x1s = (x1 * xscale) + g.xpos + (0.5 * (double) g.xsize);
						y1s = (y1 * yscale) + (double) g.ypos - (0.5 * (double) g.ysize);
						x2s = (x2 * xscale) + g.xpos + (0.5 * (double) g.xsize);
						y2s = (y2 * yscale) + (double) g.ypos - (0.5 * (double) g.ysize);
						g.drawGraphLine(g3, x1s, y1s, x2s, y2s);

						x1 += dx;
						x2 += dx;
					}
				} else {
					Line2D.Double eq_line = new Line2D.Double(x1, y1, x2, y2);		// used in fill region later
					Line2D.Double fill_line = new Line2D.Double();

					ymin = Math.min(y1, y2);		// where to stop the loop

					fx1 = g.xrange.gmin;
					fx2 = g.xrange.gmax;
					fy1 = g.yrange.gmax;

					while (fy1 >= ymin) {
						fy2 = fy1;
						fill_line.x1 = fx1;
						fill_line.y1 = fy1;
						fill_line.x2 = fx2;
						fill_line.y2 = fy2;

						Point2D.Double p = line_intersect(eq_line, fill_line);
						if (p != null) {
							// lines intersect
							if (slope >= 0) {
								fx2 = p.x;
								fy2 = p.y;
							} else {
								fx1 = p.x;
								fy1 = p.y;
							}
						}

						x1s = (fx1 * xscale) + g.xpos + (0.5 * (double) g.xsize);
						y1s = (fy1 * yscale) + (double) g.ypos - (0.5 * (double) g.ysize);
						x2s = (fx2 * xscale) + g.xpos + (0.5 * (double) g.xsize);
						y2s = (fy2 * yscale) + (double) g.ypos - (0.5 * (double) g.ysize);
						g.drawGraphLine(g3, x1s, y1s, x2s, y2s);

						fy1 -= dx;
					}

				}
			} 

		} else {
			// NavgraphExtension.debug("equation is empty");
		}
	}


	
	public String floatExpression() {
		int len = this.expression.length();
		int src_idx;
		boolean innump, isfloatp;
		StringBuffer tmp = new StringBuffer(len * 2);
		String rets;
		Character c; 
		
		innump = false;
		isfloatp = false;
		for (src_idx=0; src_idx<len; src_idx++) {
			c = this.expression.charAt(src_idx);
			if (Character.isDigit(c)) {
				innump = true;
				tmp.append(c);
			} else if (c == '.') {
				isfloatp = true;
				if (!innump) {
					tmp.append('0');
					innump = true;
				}
				tmp.append(c);
			} else {
				if (innump && !isfloatp) {
					// we were in a number and now we are not - so add the ".0" to output buf
					tmp.append('.');
					tmp.append('0');
					tmp.append(c);		// not add the actual next char
				} else {
					tmp.append(c);
				}
				innump = false;
				isfloatp = false;
			}
		}
		
		rets = new String(tmp);
		return rets;
	}
	
	
	
	
	/*
	#           P3 = seg2[0]          P2 = seg1[1]
	#         \  line b          /  
	#           \              /
	#             \          /
	#               \      /
	#                 \  /
	#                   *
	#                 /   \
	#               /       \
	#             /           \
	#   line a  /               \
	#         /                   \
	#       P1 = seg1[0]           P4 = seg2[1]
	#

	# line_intersect(seg1, seg2, segment=False)
	# from http://paulbourke.net/geometry/lineline2d/
	#
	*/
	
	public Point2D.Double line_intersect(Line2D.Double seg1, Line2D.Double seg2) {

		Point2D.Double p = new Point2D.Double();
		double x, y;		//intersection point

		double x1 = seg1.x1;
		double y1 = seg1.y1;
		double x2 = seg1.x2;
		double y2 = seg1.y2;

		double x3 = seg2.x1;
		double y3 = seg2.y1; 
		double x4 = seg2.x2; 
		double y4 = seg2.y2;

		double denom = ((y4-y3) * (x2-x1)) - ((x4-x3) * (y2-y1));

		double ua_num = ((x4-x3) * (y1-y3)) - ((y4-y3) * (x1-x3));

		double ub_num = ((x2-x1) * (y1-y3)) - ((y2-y1) * (x1-x3));

		// # lines are coincident
		if ((ua_num == 0.0) && (ub_num == 0.0) && (denom == 0.0)) {
			p.x = x1;
			p.y = y1;
			return p;
		}

		// # lines are parallel
		if (denom == 0) {
			return null;
		}  

		double ua = ua_num / denom;
		double ub = ub_num / denom;

		x = x1 + (ua * (x2 - x1));
		y = y1 + (ua * (y2 - y1));

		// looking for line segments - both ua and ub must be between 0 and 1

		if ((0.0 <= ua) && (0.0 <= ub) && (ua <= 1.0) && (ub <= 1.0)) {
			p.x = x;
			p.y = y;
			return p;
		}

		return null;
	}


	/********************************************************************************
	 *  CLASS methods
	 *******************************************************************************/

	public Equation(String eq_name, String expr, String color, String eq_type) {
		this.name = new String(eq_name.trim());
		this.expression = new String(expr.trim());		// don't mess with case on this - might get shown back to user - instead see context("x") or ("X") below
		this.color = new String(color.trim().toLowerCase());
		this.type = new String(eq_type.trim().toLowerCase());
		this.visiblep = true;
		this.activep = true;
		
		Equation.EquationList.add(this);
	}
	
	
	static public double convertToDouble(Object obj) throws Exception {
		double x;
		if (obj instanceof Double) {
			x = (double) ((Double) obj).doubleValue();
		} else if (obj instanceof Integer) {
			x = (double) ((Integer) obj).intValue();
		} else if (obj instanceof Float) {
			x = (double) ((Float) obj).floatValue();
		} else {
			x = 0.0;
			throw new Exception("Unrecognized instance type from evaluate; class = " + obj.getClass());
		}
		return x;
	}

	
}


