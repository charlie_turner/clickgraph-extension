/********************************************************************************
 * GraphObj.java: 3rd-level file for NetLogo clickgraph extension.
 *******************************************************************************/

import java.util.LinkedList;

public class GraphObj {            

	double  xloc, yloc;               	// object center
	double 	radius;						// circular object size
	// class variable for tracking all created grapher instances
	static LinkedList<GraphObj> GraphObjList = new LinkedList<GraphObj>();
	
	/********************************************************************************
	 *   INSTANCE Methods 
	 *******************************************************************************/

	public GraphObj(double txloc, double tyloc, double tradius) {
		this.xloc = txloc;
		this.yloc = tyloc;
		this.radius = tradius;		

		GraphObj.GraphObjList.add(this);
	}
}
