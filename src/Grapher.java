/********************************************************************************
 * Grapher.java: 2nd-level primitives file for NetLogo clickgraph extension.
 *		Commands in this file operate at the Grapher level.  Only one workspace
 * 		can be visible in the NetLogo world at any time.  A workspace can contain
 *		multiple graphers (usually N x M).
 *		
 *******************************************************************************/


//package org.nlogo.extensions.clickgraph;

// NetLogo extension API includes
import org.nlogo.api.LogoException;
import org.nlogo.api.ExtensionException;
import org.nlogo.api.Argument;
import org.nlogo.api.Syntax;
import org.nlogo.api.Context;
import org.nlogo.api.DefaultReporter;
import org.nlogo.api.DefaultCommand;
import org.nlogo.api.LogoListBuilder;
import org.nlogo.api.LogoList;

// generic java graphing includes
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;	// for intersection
import java.awt.Stroke;
import java.awt.BasicStroke;
import java.awt.Font;
import java.awt.FontMetrics;	// for rendering right justified equations 
import java.util.LinkedList;

import sun.misc.BASE64Decoder;	// for converting user strings to images in DrawImage
import sun.misc.CharacterDecoder;

import javax.imageio.ImageIO;	// for image display testing - reading from file
import java.io.IOException;		// for image display testing - reading from file
import java.io.File;			// for image display testing - reading from file
// for image scaling
//import static org.imgscalr.Scalr.*;
import static org.imgscalr.Scalr.resize;
import static org.imgscalr.Scalr.crop;


import java.io.Reader;			// testing 

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;


public class Grapher {            
	
	private static double DEFAULT_DIM = 10.0;		// graphs start out [-10, 10] in each dimension
	private static double DEFAULT_TICK_INT = 1.0;	// when graph is at DEFAULT_DIM, ticks this far apart
	
	String 	name;
	boolean visiblep;
	double  xpos, ypos;               	// upper left positions
	double 	xsize, ysize;				// graph sizes
	boolean axes_visiblep;				// added to hide/show axes - cjt 10/4/12
	boolean axes_labelp;				// display units along axes
	String 	axes_color;
	double	axes_weight;
	String 	background_color;
	double	zoom;						// zoom factor applied to xrange & yrange
	GraphRange xrange, yrange;			// plotting ranges
	boolean line_eq_visiblep;			// control whether two line equations are drawn 11-26-14
	String  line_eq_G;					// code specifically for Graphing in Groups 11-26-14
	String  line_eq_Y;					// 2 lines/grapher Green or Yellow, 1-4: display equations above; 5-8 below graph window

	// equations plotted in this grapher
	LinkedList<Equation> equations = new LinkedList<Equation>();
	
// TODO: Grapher.GraphObj - comment - integrate with ImageObj
	LinkedList<GraphObj> graph_objects = new LinkedList<GraphObj>();
	LinkedList<ImageObj> image_objects = new LinkedList<ImageObj>();
	
	// class variable for tracking all created grapher instances
	static LinkedList<Grapher> GrapherList = new LinkedList<Grapher>();
	
	// GraphRange: small object to allow implementation of zooming 
	// 		gmin and gmax are bounds of graph window in student X, Y coordinates
	//		by changing these will cause automatic zoom on redraw
	//
// TODO: Grapher.GraphRange() who sets these?  should coordinate conversion go on in here?
	static class GraphRange {
		double gmin, gmax, gstep;
		public GraphRange(double tmin, double tmax, double tstep) {
			this.gmin = tmin;
			this.gmax = tmax;
			this.gstep = tstep;
		}
	}
	
	
	// instance and class methods are below NetLogo primitives.
	
	/********************************************************************************
	 *  NETLOGO Extension PRIMITIVES 
	 *******************************************************************************/
	
	/********************************************************************************
	 * grapher-show: NetLogo primitive to activate and display a single grapher
	 *	inputs: grapher's name
	 *	it is an error to try to show a grapher that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/

	public static class Show extends DefaultCommand {

		public Syntax getSyntax() {
			int[] right = { Syntax.StringType() }; 	// grapher name
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {

			String name;
			Grapher g;

			// Extract arguments
			try {
				name = args[0].getString();
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}

			Clickgraph.debug("grapher-show: name:" + name);

			if ((g = findGrapherByName(name)) == null) {
				throw new ExtensionException( "grapher-show unable to find grapher named '" + name + "'" );
			}

			g.visiblep = true;
			g.redraw(context);
		}
	}


	/********************************************************************************
	 * grapher-hide: NetLogo primitive to hide/erase a single grapher
	 *	inputs: grapher_name
	 *	it is an error to try to hide a grapher that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/

	public static class Hide extends DefaultCommand {

		public Syntax getSyntax() {
			int[] right = { Syntax.StringType() }; 	// grapher name
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String name;
			Grapher g;

			try {
				name = args[0].getString();
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}

			Clickgraph.debug("grapher-hide: name:" + name);

			if ((g = findGrapherByName(name)) == null) {
				throw new ExtensionException( "grapher-hide: unable to find grapher named '" + name + "'" );
			}
			g.visiblep = false;
		}		
	}
	
	
	/********************************************************************************
	 * grapher-get-specs: NetLogo primitive to return the position and size of a grapher
	 *	input is grapher name
	 *  returns: a LogoList of doubles [upper-left xpos, upper-left ypos, xsize, ysize]
	 *	it is an error to try to get the specs of a non-existent grapher
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/
	
	public static class GetSpecs extends DefaultReporter {
		
		public Syntax getSyntax() {
			int[] right = { Syntax.StringType() }; 	// grapher name
			return Syntax.reporterSyntax(right, Syntax.ListType());
		}
		

	  	public Object report(Argument args[], Context context) throws ExtensionException, LogoException {
	    	LogoListBuilder list = new LogoListBuilder();
			Grapher g;
			String name;
			
			// Extract arguments
			try {
				name = args[0].getString();
			} 
			
			// Hope that they have the correct type
			catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}
	
			// find the grapher or throw exception
			if ((g = findGrapherByName(name)) == null) {
				throw new ExtensionException( "GetSpecs: could not find grapher [" + name + "]" );
			}
			
			// load up a list with grapher specs: [xpos, ypos, width, height]
	      	list.add(Double.valueOf(g.xpos));
			list.add(Double.valueOf(g.ypos));
			list.add(Double.valueOf(g.xsize));
			list.add(Double.valueOf(g.ysize));
	    	
	    	return list.toLogoList();
	  	}
	}
	
	
	/********************************************************************************
	 * grapher-inboundsp: NetLogo primitive to test whether point is inside grapher
	 *	input is grapher name, xpos, ypos
	 *  returns: a boolean 
	 *	it is an error to try to check bounds of a non-existent grapher
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/
	
	public static class InBoundsP extends DefaultReporter {
		
		public Syntax getSyntax() {
			int[] right = { Syntax.StringType(),	// grapher name
							Syntax.NumberType(), 	// row position
							Syntax.NumberType()};	// col position
			return Syntax.reporterSyntax(right, Syntax.BooleanType());
		}
		

	  	public Object report(Argument args[], Context context) throws ExtensionException, LogoException {
			Grapher g;
			String name;
			double row_pos, col_pos;
			
			// Extract arguments
			try {
				name 	= args[0].getString();
				col_pos = args[1].getIntValue();
				row_pos = args[2].getIntValue();
			} 
			
			// Hope that they have the correct type
			catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}
				
			// find the grapher or throw exception
			if ((g = findGrapherByName(name)) == null) {
				throw new ExtensionException( "InBoundsP: could not find grapher [" + name + "]" );
			}
						
			if ((row_pos <= g.ypos) && 			 // top edge of grapher 
				(row_pos > g.ypos - g.ysize) && // bottom edge of grapher
				(col_pos >= g.xpos) &&			 // left edge of grapher
				(col_pos < g.xpos + g.xsize)) {	 // right edge of grapher
					
				return true;
			} else {
				if (g.name.equals("PLOT1")) {
					Clickgraph.debug("inboundsp: grapher " + g.name + " IS OUT OF BOUNDS");
					Clickgraph.debug("   xpos-left: " + g.xpos + " xpos-right: " + (g.xpos + g.xsize));
					Clickgraph.debug("   ypos-top:  " + g.ypos + " ypos-bottom: " + (g.ypos - g.ysize)); 
					Clickgraph.debug(" turt: xpos: " + col_pos + " ypos: " + row_pos);
				}
			
				return false;
			}

	  	}
	}
	
	/********************************************************************************
	 * grapher-to-patch-coords: NetLogo primitive to convert grapher (x, y) to 
	 *		NetLogo patch coordinates
	 *	inputs: grapher name, xpos, ypos
	 *  returns: LogoList with (xcoord, ycoord)
	 *	it is an error to try to do conversion on a non-existent grapher
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/
	
	public static class ToPatchCoord extends DefaultReporter {
	  // take one number as input, report a list
	  public Syntax getSyntax() {
	    return Syntax.reporterSyntax(
	    new int[] {Syntax.StringType(), Syntax.NumberType(), Syntax.NumberType()}, Syntax.ListType());
	  }

	  public Object report(Argument args[], Context context) throws ExtensionException, LogoException {
	    LogoListBuilder list = new LogoListBuilder(); // create a NetLogo list for the results
		String name;
		double x, y;
		Grapher g;

	    try {
			name 	= args[0].getString(); 			// grapher name
	      	x 		= args[1].getDoubleValue();  
			y 		= args[2].getDoubleValue();  
	    } catch(LogoException e) {
	      throw new ExtensionException( e.getMessage() ) ;
	    }

		// find the grapher or throw exception
		if ((g = findGrapherByName(name)) == null) {
			throw new ExtensionException( "grapher-to-patch-coords: could not find grapher [" + name + "]" );
		}
				
		double tsize =  g.xsize / (g.xrange.gmax - g.xrange.gmin);
		x = g.xpos + (g.xsize / 2.0) + ((double) x * tsize);
		tsize =  g.ysize / (g.yrange.gmax - g.yrange.gmin);
		y = g.ypos - (0.5 * g.ysize) + ((double) y * tsize);
		
		list.add(Double.valueOf(x));
		list.add(Double.valueOf(y));

	    return list.toLogoList();
	  }
	}

	/********************************************************************************
	 * grapher-set-location: NetLogo primitive to update xpos & ypos of a single grapher
	 *	inputs: grapher_name, xpos, ypos
	 *	it is an error to try to set positions of a grapher that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/

	public static class SetLocation extends DefaultCommand {

		public Syntax getSyntax() {
			int[] right = { Syntax.StringType(), Syntax.NumberType(), Syntax.NumberType() }; 	// grapher name
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String name;
			double xpos, ypos;
			Grapher g;

			try {
				name = args[0].getString();		// grapher name
				xpos = args[1].getIntValue();
				ypos = args[2].getIntValue();
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}

			Clickgraph.debug("grapher-set-position: name:" + name + " xpos: " + xpos + " ypos: " + ypos);

			if ((g = findGrapherByName(name)) == null) {
				throw new ExtensionException( "grapher-set-position: unable to find grapher named '" + name + "'" );
			}

			g.xpos = xpos;
			g.ypos = ypos;

			g.redraw(context);
		}		
	}

	
	/********************************************************************************
	 * grapher-set-size: NetLogo primitive to update xsize & ysize of a single grapher
	 *	inputs: grapher_name, xsize, ysize
	 *	it is an error to try to set size of a grapher that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/

	public static class SetSize extends DefaultCommand {

		public Syntax getSyntax() {
			int[] right = { Syntax.StringType(), Syntax.NumberType(), Syntax.NumberType() }; 
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String name;
			double xsize, ysize;
			Grapher g;

			try {
				name = args[0].getString();		// grapher name
				xsize = args[1].getIntValue();
				ysize = args[2].getIntValue();
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}

			Clickgraph.debug("grapher-set-size: name:" + name + " xsize: " + xsize + " ysize: " + ysize);

			if ((g = findGrapherByName(name)) == null) {
				throw new ExtensionException( "grapher-set-size: unable to find grapher named '" + name + "'" );
			}

			g.xsize = xsize;
			g.ysize = ysize;
			
			g.redraw(context);
		}		
	}
	
	
	
	/********************************************************************************
	 * grapher-set-zoom: NetLogo primitive to update zoom factor of a single grapher
	 *	inputs: grapher_name, double zoom
	 *	it is an error to try to set zoom of a grapher that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/

	public static class SetZoom extends DefaultCommand {

		public Syntax getSyntax() {
			int[] right = { Syntax.StringType(), Syntax.NumberType()}; 
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String name;
			double zoom;
			Grapher g;

			try {
				name = args[0].getString();			// grapher name
				zoom = args[1].getDoubleValue();	// zoom: out: zoom < 1.0;  in: zoom > 1.0
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}

			Clickgraph.debug("grapher-set-zoom: name:" + name + " zoom: " + zoom);

			if ((g = findGrapherByName(name)) == null) {
				throw new ExtensionException( "grapher-set-zoom: unable to find grapher named '" + name + "'" );
			}

			g.set_zoom(zoom);
			g.redraw(context);
		}		
	}
	
	/********************************************************************************
	 * grapher-set-background: NetLogo primitive to update background color of a single grapher
	 *	inputs: grapher_name, string "new color"
	 *	it is an error to try to set backgrounds of a grapher that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/

	public static class SetBackground extends DefaultCommand {

		public Syntax getSyntax() {
			int[] right = { Syntax.StringType(), Syntax.StringType() }; 	// grapher name
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String name, bg_color;
			double xpos, ypos;
			Grapher g;

			try {
				name 		= args[0].getString();		// grapher name
				bg_color 	= args[1].getString();
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}

			Clickgraph.debug("grapher-set-background: name:" + name + " color: " + bg_color);

			if ((g = findGrapherByName(name)) == null) {
				throw new ExtensionException( "grapher-set-background: unable to find grapher named '" + name + "'" );
			}

			g.background_color = new String(bg_color);

			g.redraw(context);
		}		
	}
	
	
	/********************************************************************************
	 * grapher-set-window-range: NetLogo primitive to update window ranges of a single grapher
	 *	inputs: grapher_name, LogoList of [xmin, xmax, xstep, ymin, ymax, ystep]
	 *	it is an error to try to set ranges of a grapher that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/

	public static class SetWindowRange extends DefaultCommand {

		public Syntax getSyntax() {
			int[] right = { Syntax.StringType(), Syntax.ListType() };
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String name;
			LogoList list;
			double val;
			double dval;
			Grapher g;
			
			try {
				name 	= args[0].getString();		// grapher name
				list  	= args[1].getList();		// [xmin, xmax, xstep, ymin, ymax, ystep]
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}

			Clickgraph.debug("grapher-set-window-range: name:" + name + " + some list of stuff ");

			if ((g = findGrapherByName(name)) == null) {
				throw new ExtensionException( "grapher-set-window-range: unable to find grapher named '" + name + "'" );
			}


			if (list.size() != 6) {
				throw new ExtensionException( "grapher-set-window-range: invalid number of arguments: " + list.size());
			}
			
			g.xrange.gmin = (Double) list.get(0);
			g.xrange.gmax = (Double) list.get(1);
			g.xrange.gstep = (Double) list.get(2);
			g.yrange.gmin = (Double) list.get(3);
			g.yrange.gmax = (Double) list.get(4);
			g.yrange.gstep = (Double) list.get(5);

			g.redraw(context);
		}
	}
	
	
	/********************************************************************************
	 * grapher-show-axes: NetLogo primitive to update visibility of axes in a single grapher
	 *	inputs: grapher_name showp (boolean)
	 *	it is an error to try to update a grapher that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/

	public static class ShowAxes extends DefaultCommand {

		public Syntax getSyntax() {
			int[] right = { Syntax.StringType(), Syntax.BooleanType()}; 	// grapher name, show-or-not?
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String name;
			Boolean showp;
			Grapher g;

			try {
				name 		= args[0].getString();			// grapher name
				showp		= args[1].getBooleanValue();	// show axes: true or false 
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}

			Clickgraph.debug("grapher-show-axes: name:" + name + " set to visible.");

			if ((g = findGrapherByName(name)) == null) {
				throw new ExtensionException( "grapher-show-axes: unable to find grapher named '" + name + "'" );
			}

			g.axes_visiblep = showp;
			g.redraw(context);
		}		
	}
	
	
	/********************************************************************************
	 * grapher-hide-axes: NetLogo primitive to update visibility of axes in a single grapher
	 *	inputs: grapher_name 
	 *	it is an error to try to update a grapher that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/

// TODO: make obsolete
	public static class HideAxes extends DefaultCommand {

		public Syntax getSyntax() {
			int[] right = { Syntax.StringType()}; 	// grapher name
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String name;
			Grapher g;

			try {
				name 		= args[0].getString();		// grapher name
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}

			Clickgraph.debug("grapher-hide-axes: name:" + name + " set to invisible.");

			if ((g = findGrapherByName(name)) == null) {
				throw new ExtensionException( "grapher-hide-axes: unable to find grapher named '" + name + "'" );
			}

			g.axes_visiblep = false;
			g.redraw(context);
		}		
	}
	
	
	/********************************************************************************
	 * grapher-hide-axes: NetLogo primitive to update visibility of axes in a single grapher
	 *	inputs: grapher_name, boolean axes_labelp
	 *	it is an error to try to update a grapher that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/

	public static class LabelAxes extends DefaultCommand {

		public Syntax getSyntax() {
			int[] right = { Syntax.StringType(), Syntax.BooleanType()}; 	// grapher name, display_or_not
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String name;
			Boolean labelp;
			Grapher g;

			try {
				name 		= args[0].getString();			// grapher name
				labelp		= args[1].getBooleanValue();	// new value for axes_labelp flag
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}

			Clickgraph.debug("grapher-label-axes: name:" + name + " set to " + labelp);

			if ((g = findGrapherByName(name)) == null) {
				throw new ExtensionException( "grapher-label-axes: unable to find grapher named '" + name + "'" );
			}

			g.axes_labelp = labelp;
			g.redraw(context);
		}		
	}
	
	/********************************************************************************
	 * grapher-set-axes-color: NetLogo primitive to update color of axes in a single grapher
	 *	inputs: grapher_name, color
	 *	it is an error to try to update a grapher that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/

	public static class SetAxesColor extends DefaultCommand {

		public Syntax getSyntax() {
			int[] right = { Syntax.StringType(), Syntax.StringType()}; 	// grapher name & color
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String name, color;
			Grapher g;

			try {
				name 		= args[0].getString();		// grapher name
				color		= args[1].getString();
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}

			Clickgraph.debug("grapher-set-axes-color: name:" + name + " set to " + color);

			if ((g = findGrapherByName(name)) == null) {
				throw new ExtensionException( "grapher-show-axes-color: unable to find grapher named '" + name + "'" );
			}

			g.axes_color = new String(color);
			g.redraw(context);
		}		
	}
	
	/********************************************************************************
	 * grapher-set-axes-weight: NetLogo primitive to update weight of axes in a single grapher
	 *	inputs: grapher_name, double axes_weight
	 *	it is an error to try to update a grapher that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/

	public static class SetAxesWeight extends DefaultCommand {

		public Syntax getSyntax() {
			int[] right = { Syntax.StringType(), Syntax.NumberType()}; 	// grapher name & weight
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String name;
			double weight;
			Grapher g;

			try {
				name 		= args[0].getString();		// grapher name
				weight		= args[1].getDoubleValue();
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}

			Clickgraph.debug("grapher-set-axes-weight: name:" + name + " set to " + weight);

			if ((g = findGrapherByName(name)) == null) {
				throw new ExtensionException( "grapher-show-axes-weight: unable to find grapher named '" + name + "'" );
			}

			g.axes_weight = weight;
			g.redraw(context);
		}		
	}
	
	
	/********************************************************************************
	 * grapher-draw-point: NetLogo primitive to draw a point in a single grapher
	 *	inputs: grapher_name, xloc, yloc, size
	 *	it is an error to try to set size of a grapher that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/

	public static class DrawPoint extends DefaultCommand {

		public Syntax getSyntax() {
			int[] right = { Syntax.StringType(), Syntax.NumberType(), Syntax.NumberType(), Syntax.NumberType() }; 
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String name;
			double xloc, yloc, radius;
			Grapher g;

			try {
				name 	= args[0].getString();		// grapher name
				xloc 	= args[1].getDoubleValue();
				yloc 	= args[2].getDoubleValue();
				radius 	= args[3].getDoubleValue();
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}

			Clickgraph.debug("grapher-draw-point: name:" + name + " xloc: " + xloc + " yloc: " + yloc + " radius: " + radius);

			if ((g = findGrapherByName(name)) == null) {
				throw new ExtensionException( "grapher-draw-point: unable to find grapher named '" + name + "'" );
			}

			g.graph_objects.add(new GraphObj(xloc, yloc, radius));
			g.redraw(context);
		}		
	}


	/********************************************************************************
	 * grapher-draw-image: NetLogo primitive to display an image in a single grapher
	 *	inputs: grapher_name, image (as string)
	 *	it is an error to try to set size of a grapher that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/

	public static class DrawImage extends DefaultCommand {

		public Syntax getSyntax() {
			int[] right = { Syntax.StringType(), Syntax.StringType() }; 
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String name;
			String b64img_or_path, imgprefix;
			String filename = "";
			String img_data = "";
			int size;
			Grapher g;
			BufferedImage img = null;
			
			final String FILE_PREFIX = "file:";
			final String B64_PREFIX = "data:image/jpeg;base64,";
			
			try {
				name 			= args[0].getString();		// grapher name
				b64img_or_path	= args[1].getString();		// path from NetLogo user or base64 encoded image from iPhone client
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}

			if ((g = findGrapherByName(name)) == null) {
				throw new ExtensionException( "grapher-draw-image: unable to find grapher named '" + name + "'" );
			}
			
			imgprefix = b64img_or_path.substring(0, FILE_PREFIX.length()).toLowerCase();
			
			if (imgprefix.equals(FILE_PREFIX)) {
				filename = new String(b64img_or_path.substring(FILE_PREFIX.length()));
				if (filename.length() > 0) {
					try {
						Clickgraph.debug("grapher-draw-image: load JPG image file " + filename + " into " + g.name);
					    img = ImageIO.read(new File(filename));
					} catch (IOException e) {
						throw new ExtensionException( "grapher-draw-image: exception reading test image: '" + filename + "'" );
					}
				} else {
					throw new ExtensionException( "grapher-draw-image: invalid filename after file: '" + filename + "'" );
				}
			} else {
				imgprefix = b64img_or_path.substring(0, B64_PREFIX.length()).toLowerCase();
				if (imgprefix.equals(B64_PREFIX)) {
					img_data = new String(b64img_or_path.substring(B64_PREFIX.length()));
					Clickgraph.debug("grapher-draw-image: " + img_data.length() + " byte (base64) image into " + g.name);
					BufferedInputStream in = null;
					try {
						BASE64Decoder decoder = new BASE64Decoder();
						byte[] decodedBytes = decoder.decodeBuffer(img_data);
				       	img = ImageIO.read(new ByteArrayInputStream(decodedBytes));
					} catch (IOException e) {
						throw new ExtensionException( "grapher-draw-image: exception reading b64 data");
					}
				} else {
					throw new ExtensionException( "grapher-draw-image: invalid path or image prefix: '" + imgprefix + "'" );
				}
			}
			
			if (img != null) {		
				g.image_objects.add(new ImageObj(g.xpos, g.ypos, img));
				g.redraw(context);
			} else {
				throw new ExtensionException( "grapher-draw-image: failed to get a valid image");
			}
		}		
	}


	/********************************************************************************
	 * grapher-clear-images: NetLogo primitive to clear all images from a single grapher
	 *	inputs: grapher_name
	 *	it is an error to try to update a grapher that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/

	public static class ClearImages extends DefaultCommand {

		public Syntax getSyntax() {
			int[] right = { Syntax.StringType()}; 
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String name;
			Grapher g;
			
			try {
				name 			= args[0].getString();		// grapher name
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}

			if ((g = findGrapherByName(name)) == null) {
				throw new ExtensionException( "grapher-clear-images: unable to find grapher named '" + name + "'" );
			}
			
			Clickgraph.debug("grapher-clear-images: name:" + name);
			
			// hopefully this will help with garbage collection
// TODO: Grapher.ClearImages - may need to explicitly remove g.image_objects from class list prior to clearing this grapher's list
			for (ImageObj iobj: g.image_objects) {
				ImageObj.ImageObjList.remove(iobj);
			}
			
			g.image_objects.clear();
			g.redraw(context);
			
		}		
	}


	
	/********************************************************************************
	 * grapher-copy-image: NetLogo primitive to copy an image between graphers
	 *	inputs: src_grapher_name, dst_grapher_name
	 *	it is an error to try to copy images between graphers that do not exist or
	 *  do not have images
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/

	public static class CopyImage extends DefaultCommand {

		public Syntax getSyntax() {
			int[] right = { Syntax.StringType(), Syntax.StringType() }; 
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String src_name, dst_name;
			int num_src_images;
			Grapher src_g, dst_g;
			ImageObj imgobj;
			BufferedImage img = null;
			BufferedImage new_img = null;
					
			try {
				src_name 		= args[0].getString();		// src grapher name
				dst_name 		= args[1].getString();		// dst grapher name
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}

			if ((src_g = findGrapherByName(src_name)) == null) {
				throw new ExtensionException( "grapher-copy-image: unable to find grapher named '" + src_name + "'" );
			}
			
			if ((dst_g = findGrapherByName(dst_name)) == null) {
				throw new ExtensionException( "grapher-copy-image: unable to find grapher named '" + dst_name + "'" );
			}
			
			if (src_g == dst_g) {
				throw new ExtensionException( "grapher-copy-image: src and dst graphers are the same!" );
			}
			
			num_src_images = src_g.image_objects.size();
			if (num_src_images == 1) {
				imgobj = src_g.image_objects.get(0);
				dst_g.image_objects.clear();
				new_img = ImageObj.deepCopy(imgobj.image);
				dst_g.image_objects.add(new ImageObj(imgobj.xloc, imgobj.yloc, new_img));
				dst_g.redraw(context);
				Clickgraph.debug("grapher-copy-image: src_name:" + src_name + " to dst_name: " + dst_name);
			} else if (num_src_images == 0){
				Clickgraph.debug("grapher-copy-image: src_grapher: " + src_name + " has NO images to copy - ignoring");
			} else {
				Clickgraph.debug("grapher-copy-image: src_grapher: " + src_name + " has multiple images: " + num_src_images);
				throw new ExtensionException( "grapher-copy-image: " + src_name + " has multiple images: " + num_src_images + " Please notify developer!" );
			}
			
		}		
	}

	/********************************************************************************
	 * grapher-set-group-equation: NetLogo primitive to set the equation of a line
	 *	for one of the groups using the grapher - this is the string above the grapher window
	 *	inputs: src_grapher_name, group_color, equation_string
	 *******************************************************************************/

	public static class SetGroupEquation extends DefaultCommand {

		public Syntax getSyntax() {
			int[] right = { Syntax.StringType(), Syntax.StringType(), Syntax.StringType() }; 
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String g_name, eq_color, equation;
			Grapher g;
					
			try {
				g_name 		= args[0].getString();		// src grapher name
				eq_color	= args[1].getString();		// either green or yellow
				equation 	= args[2].getString();		// equation
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}

			if ((g = findGrapherByName(g_name)) == null) {
				throw new ExtensionException( "grapher-draw-group-equation: unable to find grapher named '" + g_name + "'" );
			}

			Clickgraph.debug("grapher-set-group-equation: grapher: " + g_name + " color: [" + eq_color + "] equation: " + equation);
			if (eq_color.equals("G")) {
				g.line_eq_G = new String(equation);
			} else if (eq_color.equals("Y")) {
				g.line_eq_Y = new String(equation);
			} else {
				throw new ExtensionException( "grapher-draw-group-equation: invalid equation color: " + eq_color + " wanted G or Y!");
			}

			
		}		
	}




	/********************************************************************************
	 * grapher-dump-equation: NetLogo primitive to dump all equations in this grapher 
	 * 	to debug log file.
	 *******************************************************************************/

	public static class DumpEquations extends DefaultCommand {

		public Syntax getSyntax() {
			int[] right = { Syntax.StringType() }; 
			return Syntax.commandSyntax(right);
		}

		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String g_name, eq_color, equation;
			Grapher g;
					
			try {
				g_name 		= args[0].getString();		// src grapher name
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}

			if ((g = findGrapherByName(g_name)) == null) {
				throw new ExtensionException( "grapher-dump-equation: unable to find grapher named '" + g_name + "'" );
			}

			Clickgraph.debug("grapher-dump-equation: grapher: " + g_name + " has " + g.equations.size() + " equations.");
			for (Equation eq: g.equations) {
				eq.dump();
			}
		}		
	}

	
	/********************************************************************************
	 *   INSTANCE Methods 
	 *******************************************************************************/
	
	
	/* destroy: remove anything owned by this grapher 
			currently, just a placeholder (until equations added) */
			
	void destroy() {
// TODO: Grapher.destroy() currently does nothing
	}
	
	
	/* update_pos_size: update the position and size of the grapher */
	
	void update_pos_size(double txpos, double typos, double txsize, double tysize) {
		this.xpos = txpos;
		this.ypos = typos;
		this.xsize = txsize;
		this.ysize = tysize;
	}
	
	/* update_zoom: update the various grapher params that are directly controlled by zoom */
	
	void set_zoom(double zoom) {

		// zoom: out: zoom < 1.0;  in: zoom > 1.0
		this.zoom = zoom;	
	
		// everthing done in terms of default grapher size (normally [-10, 10])
		// as we zoom in value of zoom grows > 1.0, so less of graph range shows within window
		this.xrange.gmin = -Grapher.DEFAULT_DIM / zoom;
		this.xrange.gmax = Grapher.DEFAULT_DIM / zoom;
		this.xrange.gstep = 1.0 / zoom;		// as we zoom in, zoom grows, & graph distance between ticks gets smaller

		this.yrange.gmin = -Grapher.DEFAULT_DIM / zoom;
		this.yrange.gmax = Grapher.DEFAULT_DIM / zoom;
		this.yrange.gstep = 1.0 / zoom;
	}
	
	
	/* hide: set the visiblep flag to false */
	
	void hide() {
		this.visiblep = false;
	}
	
	
	/* redraw: draw the grapher, axis, mesh */
	
	void redraw(Context context) {
		String n;
		java.awt.Color background_color;
		
		// get the drawing area from the Netlogo Context
		BufferedImage drawing = context.getDrawing();
		Graphics2D gc = drawing.createGraphics();		// graphics context
		
		gc.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
		
		if (this.visiblep) {
			
			this.draw_line_equations(gc);	// draw the text string of the equation - not the line itself

			// according to a number of sources on-line this is the safest way to manipulate the graphic context
			// never try to reset after doing things like change the clip region.
			final Graphics2D g2 = (Graphics2D) gc.create();
			try {

				// set the clipping region
				Rectangle2D rect = new Rectangle2D.Float();
				double xc1, yc1, xc2, yc2;
				// now transform
				xc1 = Clickgraph.convertXCoord(this.xpos);
				yc1 = Clickgraph.convertYCoord(this.ypos);
				xc2 = Clickgraph.convertXCoord(this.xpos + this.xsize);
				yc2 = Clickgraph.convertYCoord(this.ypos - this.ysize);

				rect.setRect((int) xc1, (int)yc1, (int)(xc2-xc1+1), (int)(yc2-yc1+1));	// add one to pick up right & bottom edges
				g2.clip(rect);
				
				n = this.name;
				
				background_color = Clickgraph.findColor(this.background_color);
				
				// background_color = new java.awt.Color(87, 87, 87); 
				if (background_color != null) {
					g2.setColor( background_color ); 
					g2.fillRect(
							(int) Clickgraph.convertXCoord(this.xpos),
							(int) Clickgraph.convertYCoord(this.ypos),
							(int) (this.xsize * Clickgraph.get_patch_size()),
							(int) (this.ysize * Clickgraph.get_patch_size())
					);
					
				} else {
					Clickgraph.debug("regraph - background_color " + this.background_color + " parsed to NULL!!!!");
				}
				
				// draw any images first
				this.draw_image_objects(g2);
				this.draw_axes(g2);

				this.draw_graph_objects(g2);
				
				this.draw_equations(g2);
				
			} finally {
				g2.dispose();
			}
		}
	}
	
	
	void draw_graph_objects(Graphics2D g2) {
		
	 	double tick;
		double x1, y1, x2, y2;
		double x_mesh_len, y_mesh_len;
		double tsize, tmax;			// tick marks 
		String n;
		
		for (GraphObj g: this.graph_objects) {
			x1 = this.xpos + 0.5 * this.xsize + g.xloc * this.zoom;
			y1 = this.ypos - 0.5 * this.ysize + g.yloc * this.zoom;
			tsize = g.radius * this.zoom;
			g2.draw(new Ellipse2D.Double(
							(int) Clickgraph.convertXCoord(x1),
							(int) Clickgraph.convertYCoord(y1),
							(int) tsize, (int) tsize)); //  rectwidth, rectheight
		}
	}
	
// TODO: Grapher.draw_image_objects():  If zoom level too big or small, get 0 sizes, blows up scalr library
// TODO:   for now, this is being handled in the NetLogo model by restricting the zoom-level 
	
	void draw_image_objects(Graphics2D g2) {
		double window_size_in_pixels;
		int scale_size;
		BufferedImage scaled_image, clipped_image;
		double xcorner, ycorner;
		
		for (ImageObj imobj: this.image_objects) {
		
			window_size_in_pixels = Clickgraph.get_patch_size() * this.xsize;
			scale_size = (int) (window_size_in_pixels * this.zoom);

			if (this.zoom <= 1.0) {	
				
				// figure out where in the grapher window we will draw the image
				// assumption is that image remains centered		
				xcorner = this.xpos + (0.5 * (this.xsize - (this.zoom * this.xsize)));
				ycorner = this.ypos - (0.5 * (this.ysize - (this.zoom * this.ysize)));
				
				// scale to the right size
				scaled_image = resize(imobj.image, scale_size);
				
				// draw it.
				g2.drawImage(scaled_image,
							(int) Clickgraph.convertXCoord(xcorner),
							(int) Clickgraph.convertYCoord(ycorner),
							null);
			} else if (this.zoom > 1.0) {
				// all the contortions below are designed to keep from using up Java's heap 
				// which can happen if one zooms in too far and simply scales the entire image accordingly
				
				// scale to exactly match window
				scaled_image = resize(imobj.image, (int) window_size_in_pixels);
				xcorner = ycorner = 0.5 * (window_size_in_pixels - (window_size_in_pixels / this.zoom));

				// now cut out the subimage that will be zoomed to fit window
				clipped_image = crop(scaled_image, (int) xcorner, (int) ycorner, (int) (window_size_in_pixels / this.zoom), (int) (window_size_in_pixels / this.zoom));
				
				// scale the subimage to match the grapher window size
				scaled_image = resize(clipped_image, (int) window_size_in_pixels);
				// draw the scaled image
				g2.drawImage(scaled_image,
							(int) Clickgraph.convertXCoord(this.xpos),
							(int) Clickgraph.convertYCoord(this.ypos),
							null);
			}		
		}
	}
	
	
	void draw_equations(Graphics2D g2) {
		// draw equations
		boolean vertical_shading = true;
		int eqidx;

		for (eqidx = 0; eqidx < this.equations.size(); eqidx++) {
		 	Equation eq = this.equations.get(eqidx);
						
			try {
				eq.draw(this, g2, vertical_shading);
			} catch(Exception e) {
				System.err.println("**** Exception Drawing Equation: " + e.getMessage());
				g2.setColor( Clickgraph.text_error_color );
				String errs = new String("Fn " + (eqidx+1) + " Error: " + eq.expression);
				double erx = (double) this.xpos + 2;
				double ery = (double) this.ypos - 3 - (eqidx * 2);
				g2.drawString(errs, (int) Clickgraph.convertXCoord(erx), (int) Clickgraph.convertYCoord(ery));	
			}
			vertical_shading = !vertical_shading;	// invert every plot
		}
	}
	
	// draw_line_equations: The textual representation above or below the graph
	void draw_line_equations(Graphics2D gc) {
		// draw equations for group 1 and 2 if they are set
		// group 1 = Green (G); group 2 = Yellow (Y)
		// graphers 1-4 get their equations drawn above, graphers 5-8 get them drawn below.
		// 11-26-14

		int eq_yoff_y, eq_yoff_g;
		Boolean is_plot;
		String prefix;
		int name_len;
		int plot_num;

		if (this.line_eq_visiblep) {

			double right_edge = Clickgraph.convertXCoord(this.xpos + this.xsize);
			// Font defaultFont = new Font("Helvetica", Font.PLAIN, 12);
			// FontMetrics fontMetrics = new FontMetrics(defaultFont){};	// the {} at the end fixes the abstract type error
			FontMetrics fm = gc.getFontMetrics();		// use fontMetrics to compute width of each equation
			//g.drawString(s, rightEdge - fontMetrics.stringWidth(s), y);

			// calculate Y coordinate where string will be drawn.  This is TOTALLY hardwired to Graphing in Groups
			// which assumes two rows of 4 graphers, drawing the equations of the top row above the graphers, and 
			// equations of the bottom row below the graphers.  This way the graph windows are as close together as possible
			// in the middle.

			// set up for the default case of drawing above the graphs
			eq_yoff_g = (int) Clickgraph.convertYCoord(this.ypos + 1);
			eq_yoff_y = (int) Clickgraph.convertYCoord(this.ypos + 3);

			// if this is a grapher of the form: PLOT1, PLOT2, ..., PLOT8 then do a special check for 
			// bottom row PLOT5 - PLOT8, adjusting the y-offset so that labels go below the grapher windows
			if (this.name.length() == 5) {
				prefix = this.name.substring(0, 4);		// check to see if prefix == PLOT
				if (prefix.equals("PLOT")) {
					plot_num = (int) Integer.parseInt(this.name.substring(4, 5));	// extract plot number
					if (plot_num >= 5 && plot_num <= 8) {
						eq_yoff_g = (int) Clickgraph.convertYCoord(this.ypos - this.ysize - 2);
						eq_yoff_y = (int) Clickgraph.convertYCoord(this.ypos - this.ysize - 4);
					}
				}
			}
			
			gc.setColor(Clickgraph.green_line_color);
			gc.drawString(this.line_eq_G, (int) right_edge - fm.stringWidth(this.line_eq_G), eq_yoff_g);
			gc.setColor( Clickgraph.yellow_line_color);
			gc.drawString(this.line_eq_Y, (int) right_edge - fm.stringWidth(this.line_eq_Y), eq_yoff_y); 	

		}
	}

	
	void draw_axes (Graphics2D g2) {
		
	 	double tick;
		double x1, y1, x2, y2;
		double x_mesh_len, y_mesh_len;
		double tsize, tmax;			// tick marks 
		String n;
			
		// abide by axes_visiblep & axes_color (even on ticks) 
		if (this.axes_visiblep) {

			// draw tick marks on horizontal/X axis
			if (Clickgraph.draw_meshp) {
				g2.setColor( Clickgraph.mesh_color );
				x_mesh_len = this.xsize * 0.5;						// cover the whole grid
				y_mesh_len = this.ysize * 0.5;
			} else {
				g2.setColor( Clickgraph.findColor(this.axes_color));						// these are really tick marks
				x_mesh_len = this.xsize * 0.01;						// along axes only - same color as axes
				y_mesh_len = this.ysize * 0.01;
			}
									
			// draw horizontal tick marks
			tsize = (this.xsize * this.xrange.gstep) / (this.xrange.gmax - this.xrange.gmin) ;
			tmax = (this.xrange.gmax - this.xrange.gmin + 1) / this.xrange.gstep;

			// y positions stay constant for all x-ticks
			y1 = (double) this.ypos - (0.5 * (double) this.ysize) + y_mesh_len;
			y2 = (double) this.ypos - (0.5 * (double) this.ysize) - y_mesh_len;
			for (tick = 0; tick < tmax; tick++) {
				x1 = this.xpos + (tick * tsize);
				x2 = this.xpos + (tick * tsize);
				drawGraphLine(g2, x1, y1, x2, y2);
			}

			// draw tick marks on vertical / Y axis				
			tsize = (this.ysize * this.yrange.gstep) / (this.yrange.gmax - this.yrange.gmin);
			tmax = (this.yrange.gmax - this.yrange.gmin + 1) / this.yrange.gstep;
			
			// x positions stay constant for all y-ticks
			x1 = (double) this.xpos + (0.5 * (double) this.xsize) + x_mesh_len;
			x2 = (double) this.xpos + (0.5 * (double) this.xsize) - x_mesh_len;
			for (tick = 0; tick < tmax; tick++) {
				y1 = this.ypos - (tick * tsize);
				y2 = this.ypos - (tick * tsize);
				drawGraphLine(g2, x1, y1, x2, y2);
			}
			
			// bug fix: make sure independent axes_color is used - rather than just stoke_color - cjt 10/4/12
			g2.setColor(Clickgraph.findColor(this.axes_color));
		
// TODO: Grapher.draw_axes - add comments
			g2.setStroke(new BasicStroke((int) this.axes_weight));

			// draw vertical axis
			x1 = (double) this.xpos + (0.5 * (double) this.xsize);
			y1 = (double) this.ypos;
			x2 = (double) this.xpos + (0.5 * (double) this.xsize);
			y2 = (double) this.ypos - this.ysize;
			drawGraphLine(g2, x1, y1, x2, y2);

			// draw horizontal axis
			x1 = (double) this.xpos;
			y1 = (double) this.ypos - (0.5 * (double) this.ysize);
			x2 = (double) this.xpos + this.xsize;
			y2 = (double) this.ypos - (0.5 * (double) this.ysize);
			drawGraphLine(g2, x1, y1, x2, y2);


			if (this.axes_labelp) {
				drawAxesLabels(g2);
			}
			// bug fix: go back to stroke_color being used before we set axes_color above.  This may not
			// be strictly necessary, but it it the safest way to make sure behavior remains constant.  cjt 10/4/12
			g2.setColor( Clickgraph.stroke_color );
		}

		// drow bounding box and axes on top of mesh
		g2.setColor( Clickgraph.stroke_color );
		g2.setStroke( Clickgraph.stroke );			

		// draw bounding rectangle
		g2.drawRect(
				(int) Clickgraph.convertXCoord(this.xpos),
				(int) Clickgraph.convertYCoord(this.ypos),
				(int) (this.xsize * Clickgraph.get_patch_size()),
				(int) (this.ysize * Clickgraph.get_patch_size())
		);
	}
	
	
	void drawGraphLine(Graphics2D g2, double x1, double y1, double x2, double y2) {
		int xCor1, yCor1, xCor2, yCor2;
		// now transform
		xCor1 = (int) Clickgraph.convertXCoord(x1);
		yCor1 = (int) Clickgraph.convertYCoord(y1);
		xCor2 = (int) Clickgraph.convertXCoord(x2);
		yCor2 = (int) Clickgraph.convertYCoord(y2);
	
		// draw
		g2.drawLine(xCor1, yCor1, xCor2, yCor2);
		
	}
	
	void drawAxesLabels(Graphics2D g2) {
		double dx, dy;
		double xlval, xrval, ytval, ybval;
		double lab_xpos, lab_ypos;
		String s1, s2;
		
		//static Font font = new Font("SansSerif", Font.PLAIN, 12);
		
		g2.setFont(new Font("SansSerif", Font.BOLD, 10));
		Clickgraph.debug("axes xrange.gmin = " + this.xrange.gmin + " gmax = " + this.xrange.gmax);
		// for now, making the assumption that graphs are centered at zero!
		// i.e. gmax is positive and gmin is negative
		dx = this.xrange.gmax - this.xrange.gmin;
		
		xlval = this.xrange.gmin + 0.25 * dx;
		xrval = this.xrange.gmin + 0.75 * dx;

		if(xlval == (int) xlval)
	        s1 = String.format("%d",(int) xlval);
	    else
	        s1 = String.format("%2.1f", xlval);
	
		if(xrval == (int) xrval)
	        s2 = String.format("%d",(int) xrval);
	    else
	        s2 = String.format("%2.1f", xrval);
	
		Clickgraph.debug("drawing axes dx = " + dx );
		Clickgraph.debug("   x left val: " + xlval + "  x right val: " + xrval);
		
		lab_xpos = this.xpos + (0.20 * (double) this.xsize);	// compensate for minus sign
		lab_ypos = this.ypos - (0.58 * (double) this.ysize);
		g2.drawString(s1, (int) Clickgraph.convertXCoord(lab_xpos), (int) Clickgraph.convertYCoord(lab_ypos));
		
		lab_xpos = this.xpos + (int) (0.75 * (double) this.xsize);
		g2.drawString(s2, (int) Clickgraph.convertXCoord(lab_xpos), (int) Clickgraph.convertYCoord(lab_ypos));	
		
		
		// draw the Y vals
		dy = this.yrange.gmax - this.yrange.gmin;
		
		ytval = this.yrange.gmax - 0.25 * dy;
		ybval = this.yrange.gmax - 0.75 * dy;

		if(ytval == (int) ytval)
	        s1 = String.format("%2d",(int) ytval);
	    else
	        s1 = String.format("%2.1f", ytval);
	
		if(ybval == (int) ybval)
	        s2 = String.format("%2d",(int) ybval);
	    else
	        s2 = String.format("%2.1f", ybval);
	
		Clickgraph.debug("drawing axes dy = " + dy);
		Clickgraph.debug("   y top val: " + ytval + "  y bottom val: " + ybval);
		
		lab_xpos = this.xpos + (0.52 * (double) this.xsize);	// compensate for minus sign
		lab_ypos = this.ypos - (0.28 * (double) this.ysize);
		g2.drawString(s1, (int) Clickgraph.convertXCoord(lab_xpos), (int) Clickgraph.convertYCoord(lab_ypos));
		
		lab_ypos = this.ypos - (int) (0.78 * (double) this.ysize);
		g2.drawString(s2, (int) Clickgraph.convertXCoord(lab_xpos), (int) Clickgraph.convertYCoord(lab_ypos));	
		
		
	}
	
	/* dump: output basic workspace information to stdout */
	void dump () {
		Clickgraph.debug("   ---- Grapher ----");
		Clickgraph.debug("   " + this.name + " pos: " + this.xpos + "," + this.ypos);		
	}
	
	

	/********************************************************************************
	 *  CLASS methods
	 *******************************************************************************/
	
	public Grapher(String tname, double txpos, double typos, double xsize, double ysize) {
		this.name = new String(tname.trim());
		this.xpos = txpos;
		this.ypos = typos;
		this.xsize = xsize;				// normally x and y dimensions are equal
		this.ysize = ysize;				// but they don't have to be
		this.visiblep = false;			// everything starts off invisible - so that they can be created at setup
		this.axes_visiblep = true;		// added to hide/show axes
		this.axes_color = "white";
		this.axes_labelp = false;				// display units along axes
		this.background_color = "black";
		this.axes_weight = 1.5;
		this.zoom = 1.0;				// out: zoom < 1.0;  in: zoom > 1.0
// TODO: Grapher() GrapheRanges - are these defaults good?
		this.xrange = new GraphRange(-(Grapher.DEFAULT_DIM), Grapher.DEFAULT_DIM, Grapher.DEFAULT_TICK_INT);	
		this.yrange = new GraphRange(-(Grapher.DEFAULT_DIM), Grapher.DEFAULT_DIM, Grapher.DEFAULT_TICK_INT);

		this.line_eq_visiblep = false;	// 11-26-14
		this.line_eq_G = "";			// 11-26-14
		this.line_eq_Y = "";

		// this.equations.clear();

		if (findGrapherByName(this.name) != null) {
			Clickgraph.debug(" ");
			Clickgraph.debug("******* Grapher Create - grapher with name " + this.name + " ALREADY EXISTS ********");
// TODO: Grapher: add declarations to call stack so that we can throw exception here
			//throw new ExtensionException( "Grapher: grapher with name [" + name + "] already exists!" );
			Clickgraph.debug(" ");
		}

		Grapher.GrapherList.add(this);
	}
	
	
	public static void initGraphers() {
		GrapherList.clear();
	}
	
	public static Grapher findGrapherByName(String name) {
		
		for (Grapher g: GrapherList) {
			if (name.equals(g.name)) {
				return g;
			}
	    }
	
		Clickgraph.debug("Grapher.findGrapherByName: Unable to find graph by name '" + name + "'");
	    return null;
	}
	


}
