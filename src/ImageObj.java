/********************************************************************************
 * ImageObj.java: 3rd-level file for NetLogo clickgraph extension.
 *******************************************************************************/

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;

import java.util.LinkedList;

public class ImageObj {            

	double  xloc, yloc;               	// object center
	int	width;							// width of original uploaded image
	int height;							// height of original uploaded image
	BufferedImage image;
	
	// class variable for tracking all created grapher instances
	static LinkedList<ImageObj> ImageObjList = new LinkedList<ImageObj>();
	
	
	/********************************************************************************
	 *   INSTANCE Methods 
	 *******************************************************************************/

	public ImageObj(double txloc, double tyloc, BufferedImage img) {
		this.xloc = txloc;
		this.yloc = tyloc;
		this.width = img.getWidth();
		this.height = img.getHeight();
		this.image = img;
		
		ImageObj.ImageObjList.add(this);
	}
	
	public static BufferedImage deepCopy(BufferedImage bi) {
	 	ColorModel cm = bi.getColorModel();
	 	boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
	 	WritableRaster raster = bi.copyData(null);
	 	return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
	}
	
}
