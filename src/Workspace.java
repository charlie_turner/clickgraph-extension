/********************************************************************************
 * Workspace.java: 2nd-level primitives file for NetLogo clickgraph extension.
 *		Commands in this file operate at the workspace level.  Only one workspace
 * 		can be visible in the NetLogo world at any time.  A workspace can contain
 *		N x M graphers.
 *		
 * See Grapher.java for the next level down in the object hieararchy.
 *******************************************************************************/


//package org.nlogo.extensions.clickgraph;

import org.nlogo.api.LogoException;
import org.nlogo.api.ExtensionException;
import org.nlogo.api.Argument;
import org.nlogo.api.Syntax;
import org.nlogo.api.Context;
import org.nlogo.api.DefaultReporter;
import org.nlogo.api.DefaultCommand;
import org.nlogo.api.LogoListBuilder;

// used for linked list of workspaces at the top-level
import java.util.LinkedList;
import java.util.ArrayList;		// for list of margin-specifiers
import java.util.List;
import java.util.Arrays;

// graphing support
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.RenderingHints;
import java.awt.Color;

// other
import java.lang.Math.*;



public class Workspace {
	
	private static int DEFAULT_MARGIN_SIZE = 1;			// default is minimal margin
	private static int MAX_MARGIN_SIZE = 30;			// maximum size of workspace's interior margin
	private static double DEFAULT_PADDING_SIZE = 0.5;	// default for padding between graphers
	private static double MAX_PADDING_SIZE = 10.0;		// maximum size of padding around each grapher
	
	// MarginSpec: enum used to translate between user-supplied strings "top", "bottom", etc. and
	// 		array positions in Workspace.margins[4].  
	// !! Very important that first four specifiers TOP..LEFT (in CSS order but don't need to be)
	//	retain the values 0..3, as their values are used as indicies in workspace.margins[4]
	private static enum MarginSpec {
	        TOP(0), RIGHT(1), BOTTOM(2), LEFT(3), HORIZONTAL(4), VERTICAL(5), ALL(6);
	        private int value;

	        private MarginSpec(int value) {
	                this.value = value;
	        }
			public int getValue() { return value; }
			
			public static boolean contains(String test) {
				String uptest = test.toUpperCase();
			    for (MarginSpec ms : MarginSpec.values()) {
			        if (ms.name().equals(uptest)) {
			            return true;
			        }
			    }
			    return false;
			}
	};   

	private int[] margins = new int[4];	// interior margins of workspace; default=0, set with workspace-set-margin 

	private String 	name;				// user supplied name of workspace and prefix of grapher names
	private int		rows, cols;			// dimensions of the workspace in graph windows - not patches or pixels
	private double 	padding;			// 1/2 amount of padding between each grapher window
	private boolean visiblep;			// only one workspace can be visible at a time
	private String 	background_color;	// not sure if this is needed.
	private boolean line_eq_visiblep;	// all graphers in this workspace display equations of lines above graph? 11-26-14

	// instance variable for all subordinate grapher windows in this workspace
	private LinkedList<Grapher> graphers = new LinkedList<Grapher>();
	
	// class variable for tracking all created workspace instances
	private static LinkedList<Workspace> WorkspaceList = new LinkedList<Workspace>();
	
	// instance and class methods are below NetLogo primitives.
		
	/********************************************************************************
	 *  NETLOGO Extension PRIMITIVES 
	 *******************************************************************************/

	/********************************************************************************
	 * workspace-create: NetLogo primitive to perform workspace creation
	 *	inputs are name and layout of workspace (rows x cols of graphers)
	 *	it is an error to create workspaces with duplicate names - this will cause
	 *	an exception to be thrown.
	 *******************************************************************************/
	
	public static class Create extends DefaultCommand {
		
		public Syntax getSyntax() {
			int[] right = { Syntax.StringType(), 	// workspace name
							Syntax.NumberType(), 	// rows
							Syntax.NumberType()};	// cols
			return Syntax.commandSyntax(right);
		}
	
		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			
			String name;
			int rows, cols;
			
			// Extract arguments
			try {
				name = args[0].getString();
				rows = args[1].getIntValue();
				cols = args[2].getIntValue();
			} 
			
			// Hope that they have the correct type
			catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}
			
			Clickgraph.debug("workspace-create: name:" + name + " rows: " + rows + " cols: " + cols);
			
			if (findWorkspaceByName(name) != null) {
				throw new ExtensionException( "There is already a Workspace named '" + name + "'" );
			}
			
			Workspace ws = new Workspace(name, rows, cols);
		}
	}
	
	
	/********************************************************************************
	 * workspace-destroy: NetLogo primitive to perform workspace deletion
	 *	input: name of workspace 
	 *	it is an error to destroy non-existent workspaces - this will cause
	 *	an exception to be thrown.
	 *******************************************************************************/
	
	public static class Destroy extends DefaultCommand {
		
		public Syntax getSyntax() {
			int[] right = { Syntax.StringType()}; 	// workspace name
			return Syntax.commandSyntax(right);
		}
	
		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			Workspace ws;
			String name;
			
			// Extract arguments
			try {
				name = args[0].getString();
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}
			
			Clickgraph.debug("workspace-destory: name:" + name);
			
			if ((ws = findWorkspaceByName(name)) == null) {
				throw new ExtensionException( "workspace-destory: unable to find workspace named '" + name + "'" );
			}
			
			// remove all graphers from this workspace
			for (Grapher g: ws.graphers) {
	      		g.destroy();
			}
			// remove workspace itself from class list of workspaces
			Workspace.WorkspaceList.remove(ws);
			
		}
	}
	
	
	
	/********************************************************************************
	 * workspace-show: NetLogo primitive to activate and display a workspace
	 *	inputs are workspace_name
	 *	it is an error to try to show a workspace that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/
	
	public static class Show extends DefaultCommand {
		
		public Syntax getSyntax() {
			int[] right = { Syntax.StringType() }; 	// workspace name
			return Syntax.commandSyntax(right);
		}
	
		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			
			String name;
			Workspace ws;
			
			// Extract arguments
			try {
				name = args[0].getString();
			} 
			
			// Hope that they have the correct type
			catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}
			
			Clickgraph.debug("workspace-show: name:" + name);
			
			if ((ws = findWorkspaceByName(name)) == null) {
				throw new ExtensionException( "workspace-show unable to find workspace named '" + name + "'" );
			}
			
			ws.visiblep = true;
			// Clickgraph.eraseWorld(context);		// must pass the graphic context for all drawing
			for (Grapher g: ws.graphers) {
				g.visiblep = true;
		    }
			
			ws.redraw(context);
		}
	}
	
	
	/********************************************************************************
	 * workspace-hide: NetLogo primitive to hide/erase a workspace
	 *	inputs: workspace_name
	 *	it is an error to try to hide a workspace that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/
	
	public static class Hide extends DefaultCommand {
		
		public Syntax getSyntax() {
			int[] right = { Syntax.StringType() }; 	// workspace name
			return Syntax.commandSyntax(right);
		}
	
		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			String name;
			Workspace ws;
			
			try {
				name = args[0].getString();
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}
			
			Clickgraph.debug("workspace-hide: name:" + name);
			
			if ((ws = findWorkspaceByName(name)) == null) {
				throw new ExtensionException( "workspace-hide: unable to find workspace named '" + name + "'" );
			}
			
			ws.visiblep = false;
			
			for (Grapher g: ws.graphers) {
				g.visiblep = false;
		    }
			
			ws.redraw(context); 

		}
	}
		
	
	/********************************************************************************
	 * workspace-set-margin: NetLogo primitive to perform set one, two, or all margins
	 *	inputs are workspace name (string), margin specifier (string), and size (int)
	 *
	 *  margin-specifiers: one of ["left", "right", "top", "bottom", 
	 *								"veritical", "horizontal", "all"]
	 *	it is an error to set margins for a non-existent workspace - this will cause
	 *	an exception to be thrown.
	 *******************************************************************************/
	
	public static class SetMargin extends DefaultCommand {
				
		public Syntax getSyntax() {
			int[] right = { Syntax.StringType(), 	// workspace name
							Syntax.StringType(), 	// margin-specifier
							Syntax.NumberType()};	// margin-size
			return Syntax.commandSyntax(right);
		}
	
		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			Workspace ws;
			String name, margin_spec;
			int margin_size;
			
			// Extract arguments
			try {
				name 		= args[0].getString();
				margin_spec = args[1].getString().toLowerCase();
				margin_size	= args[2].getIntValue();
			} 
			
			// Hope that they have the correct type
			catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}
			
			Clickgraph.debug("workspace-set-margin: name:" + name + " margin_spec: " + margin_spec + " margin_size: " + margin_size);
			
			if ((ws = findWorkspaceByName(name)) == null) {
				throw new ExtensionException( "workspace-set-margin: unable to find workspace named '" + name + "'" );
			}
			
			if (!MarginSpec.contains(margin_spec)) {
				throw new ExtensionException( "workspace-set-margin: invalid margin-specification '" + margin_spec + "'" );
			}
			
			if (margin_size < 0 || margin_size > MAX_MARGIN_SIZE) {
				throw new ExtensionException( "workspace-set-margin: margin-size needs to be between 0 and " + MAX_MARGIN_SIZE );
			}
			// have workspace and valid parameters
			ws.updateMargins(margin_spec, margin_size);
			
			// redraw
			ws.updateGraphers();
		}
	}
	
	
	/********************************************************************************
	 * workspace-set-padding: NetLogo primitive to perform set of grapher padding
	 *	inputs are workspace name (string), and padding (double)
	 *
	 *	it is an error to set padding for a non-existent workspace - this will cause
	 *	an exception to be thrown.
	 *******************************************************************************/
	
	public static class SetPadding extends DefaultCommand {
				
		public Syntax getSyntax() {
			int[] right = { Syntax.StringType(), 	// workspace name
							Syntax.NumberType()};	// padding-size
			return Syntax.commandSyntax(right);
		}
	
		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			Workspace ws;
			String name, margin_spec;
			double padding_size;
			
			// Extract arguments
			try {
				name 		 = args[0].getString();
				padding_size = args[1].getDoubleValue();
			} 
			
			// Hope that they have the correct type
			catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}
			
			Clickgraph.debug("workspace-set-padding: name:" + name + " padding_size: " + padding_size);
			
			if ((ws = findWorkspaceByName(name)) == null) {
				throw new ExtensionException( "workspace-set-padding: unable to find workspace named '" + name + "'" );
			}
			
			
			if (padding_size < 0.0 || padding_size > MAX_PADDING_SIZE) {
				throw new ExtensionException( "workspace-set-padding: padding-size needs to be between 0.0 and " + MAX_PADDING_SIZE );
			}
			// have workspace and valid parameters - update padding
			ws.padding = padding_size;
			
			ws.updateGraphers();		// redraw
		}
	}
	

	/********************************************************************************
	 * workspace-show-line-equations: NetLogo primitive to activate/deactivate display of grapher 
	 * 	line equations. This is specifically for Graphing in Groups.  11-26-14
	 *	inputs are workspace_name and show-equations = True/False 
	 *	it is an error to try to update for a workspace that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/
	
	public static class ShowLineEquations extends DefaultCommand {
		
		public Syntax getSyntax() {
			int[] right = { Syntax.StringType(), Syntax.BooleanType() }; 	// workspace name
			return Syntax.commandSyntax(right);
		}
	
		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			
			String name;
			Boolean showornot;
			Workspace ws;
			
			// Extract arguments
			try {
				name = args[0].getString();
				showornot = args[1].getBoolean();
			} 
			
			// Hope that they have the correct type
			catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}
			
			Clickgraph.debug("workspace-show-line-equations: name:" + name + " showornot: " + showornot);
			
			if ((ws = findWorkspaceByName(name)) == null) {
				throw new ExtensionException( "workspace-show unable to find workspace named '" + name + "'" );
			}
			
			ws.line_eq_visiblep = showornot;
			for (Grapher g : ws.graphers) {
				g.line_eq_visiblep = showornot; // update this workspace's graphers  11-26-14
			}

			// we did something that might impact display - so call top-level regraph 11-26-14
			Regraph(context);
		}
	}
	
	/********************************************************************************
	 * workspace-show-lines: NetLogo primitive to activate/deactivate display of grapher 
	 * 	lines. 
	 *	inputs are workspace_name and show-equations = True/False 
	 *	it is an error to try to update for a workspace that has not been previously created
	 *	 - this will cause an exception to be thrown.
	 *******************************************************************************/
	
	public static class ShowLines extends DefaultCommand {
		
		public Syntax getSyntax() {
			int[] right = { Syntax.StringType(), Syntax.BooleanType() }; 	// workspace name
			return Syntax.commandSyntax(right);
		}
	
		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			
			String name;
			Boolean showornot;
			Workspace ws;
			
			// Extract arguments
			try {
				name = args[0].getString();
				showornot = args[1].getBoolean();
			} 
			
			// Hope that they have the correct type
			catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}
			
			Clickgraph.debug("workspace-show-lines: name:" + name + " showornot: " + showornot);
			
			if ((ws = findWorkspaceByName(name)) == null) {
				throw new ExtensionException( "workspace-show unable to find workspace named '" + name + "'" );
			}
			
			for (Grapher g : ws.graphers) {
				for (Equation eq : g.equations) {
					eq.visiblep = showornot;
				}
			}

			// we did something that might impact display - so call top-level regraph 11-26-14
			Regraph(context);
		}
	}
	

	
	/********************************************************************************
	 * workspace-add-grapher: NetLogo primitive to create a new grapher
	 *	inputs: workspace name (string), 
	 *		grapher name, xpos, ypos, xsize, ysize
	 *
	 *	it is an error to add a grapher to a non-existent workspace - 
	 *	it is also an error to add a grapher with a pre-existing name 
	 *	these conditions will cause an exception to be thrown.
	 *******************************************************************************/
	
	public static class AddGrapher extends DefaultCommand {
				
		public Syntax getSyntax() {
			int[] right = { Syntax.StringType(), 	// workspace name
							Syntax.StringType(), 	// grapher name
							Syntax.NumberType(),	// xpos
							Syntax.NumberType(),	// ypos
							Syntax.NumberType(),	// xsize
							Syntax.NumberType()};	// ysize
			return Syntax.commandSyntax(right);
		}
	
		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			Workspace ws;
			Grapher gr;
			String ws_name, gr_name;
			double xpos, ypos, xsize, ysize;
			
			// Extract arguments
			try {
				ws_name 	= args[0].getString();
				gr_name 	= args[1].getString();
				xpos	 	= args[2].getDoubleValue();
				ypos	 	= args[3].getDoubleValue();
				xsize	 	= args[4].getDoubleValue();
				ysize	 	= args[5].getDoubleValue();
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}
			
			Clickgraph.debug("workspace-add-grapher: adding (" + xsize + " x " + ysize + ") grapher:" + gr_name + " to workspace: " + ws_name);
			
			if ((ws = findWorkspaceByName(ws_name)) == null) {
				throw new ExtensionException( "workspace-add-grapher: unable to find workspace named '" + ws_name + "'" );
			}
			
			if (ws.findGrapherByName(gr_name) != null) {
				throw new ExtensionException( "workspace-add-grapher: there is already a grapher named " + gr_name + " in workspace " + ws_name);
				// we create square graphers - but they need not be
			} 
				
			ws.graphers.add(new Grapher(gr_name, xpos, ypos, xsize, ysize));
			
			ws.updateGraphers();		// redraw
		}
	}
	
	
	/********************************************************************************
	 * workspace-get-grapher-names: NetLogo primitive to return the names of all
	 *	graphers in this workspace
	 *	inputs: workspace name (string)
	 *  returns: a LogoList of strings ["grapher-1-name", "grapher-2-name" ...]
	 *******************************************************************************/
	
	public static class GetGrapherNames extends DefaultReporter {
		
		public Syntax getSyntax() {
			int[] right = {Syntax.StringType()}; 	// workspace name
			return Syntax.reporterSyntax(right, Syntax.ListType());
		}
		

	  	public Object report(Argument args[], Context context) throws ExtensionException, LogoException {
	    	LogoListBuilder list = new LogoListBuilder();
			Workspace ws;
			String name;
			
			// Extract name or throw exception
			try {
				name = args[0].getString();
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}
			
			// find the workspace or throw exception
			if ((ws = findWorkspaceByName(name)) == null) {
				throw new ExtensionException( "workspace-get-grapher-names: could not find workspace [" + name + "]" );
			}
			
			// loop over all graphers in this workspace, append the grapher name to return list
			for (Grapher g: ws.graphers) {
	      		list.add(new String(g.name));
			}
	    	
	    	return list.toLogoList();
	  	}
	}
	
	
	/********************************************************************************
	 * workspace-get-num-graphers: NetLogo primitive to return the number of 
	 *	graphers in this workspace
	 *	inputs: workspace name (string)
	 *  returns: a NumberType
	 *******************************************************************************/
	
	public static class GetNumGraphers extends DefaultReporter {
		
		public Syntax getSyntax() {
			int[] right = {Syntax.StringType()}; 	// workspace name
			return Syntax.reporterSyntax(right, Syntax.NumberType());
		}
		

	  	public Object report(Argument args[], Context context) throws ExtensionException, LogoException {
			Workspace ws;
			String name;
			double rval;
			
			// Extract name or throw exception
			try {
				name = args[0].getString();
			} catch( LogoException e ) {
				throw new ExtensionException( e.getMessage() );
			}
			
			// find the workspace or throw exception
			if ((ws = findWorkspaceByName(name)) == null) {
				throw new ExtensionException( "workspace-get-num-graphers: could not find workspace [" + name + "]" );
			}
	    	
			Clickgraph.debug("workspace-get-num-graphers: " + (ws.rows * ws.cols));
			rval = ws.rows * ws.cols;
	    	return rval;
	  	}
	}
	
	/********************************************************************************
	 * dump-workspaces: NetLogo primitive to perform hierarchical dump of workspaces
	 *		and their subordinagte graphs, lines, points, equations
	 *		output just goes to stdout, so NetLogo must be run from console to see
	 *		at least on a Mac.
	 *******************************************************************************/
	
	public static class DumpWorkspaces extends DefaultCommand {
		
		public Syntax getSyntax() {
			int[] right = { };	// no arguments
			return Syntax.commandSyntax(right);
		}
	
		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			Clickgraph.debug("Dump of all workspaces: ");
			if (WorkspaceList.isEmpty()) {
				Clickgraph.debug("  WorkspaceList is EMPTY!");
			} else {
				for(Workspace ws : WorkspaceList) {
					ws.dump();
				}
			}
		}
	}
	
	/********************************************************************************
	 * clear-workspaces: NetLogo primitive to erase and delete all workspaces 
	 *		and their subordinagte graphs, lines, points, equations
	 *******************************************************************************/
	
	public static class ClearWorkspaces extends DefaultCommand {
		
		public Syntax getSyntax() {
			int[] right = { };	
			return Syntax.commandSyntax(right);
		}
	
		public void perform(Argument args[], Context context) throws ExtensionException, LogoException {
			Clickgraph.debug("clearing all workspaces: ");
			initWorkspaces();
		}
	}
	
	/********************************************************************************
	 *  INSTANCE methods
	 *******************************************************************************/
	
	/* create a Workspace instance given name, rows, and cols */
	public Workspace(String tname, int trows, int tcols) {
		this.name = new String(tname.trim());
		this.rows = trows;
		this.cols = tcols;
		this.visiblep = false;
		this.line_eq_visiblep = true;			// graphs display line equations above graph 11-26-14
		this.padding = DEFAULT_PADDING_SIZE;	// can be updated with SetPadding
// TODO: Workspace.Workspace() - see if eraseWorld - fill_color should be handled here.
		this.background_color = "black";		
		// margin defaults can be overridden with workspace-set-margin
		this.margins[MarginSpec.TOP.getValue()] = DEFAULT_MARGIN_SIZE;
		this.margins[MarginSpec.BOTTOM.getValue()] = DEFAULT_MARGIN_SIZE;	
		this.margins[MarginSpec.LEFT.getValue()] = DEFAULT_MARGIN_SIZE;
		this.margins[MarginSpec.RIGHT.getValue()] = DEFAULT_MARGIN_SIZE;
						
		this.updateGraphers();
		
		// add self to class variable list of all workspaces
		WorkspaceList.add(this);
	}
	
	private Grapher findGrapherByName(String name) {
		Clickgraph.debug("Workspace.findGrapherByName: search workspace " + this.name + " for grapher '" + name + "'");
		for (Grapher g: this.graphers) {
			//Clickgraph.debug("  checking " + g.name);
			if (name.equals(g.name)) {
				//Clickgraph.debug("*** Graph " + g.name + " matches");
				return g;
			}
	    }
	
		Clickgraph.debug("Workspace.findGrapherByName: Unable to find graph by name '" + name + "'");
	    return null;
	}	
	
	
	private void updateGraphers() {
		
		int r, c, gnum;
		String gname;
		Grapher g;
		Boolean numberingp = true;  // use numbering for grapher names? see comment below for details
		
// TODO: Workspace.UpdateGraphers() for the moment, nuke all the graphers in this workspace.  This may be more radical than necessary - commented out
		// this.graphers.clear();
		
		// given size of world and number of rows/cols in workspace, determine the grapher dimensions
		int world_width 	= Clickgraph.get_world_width();
		int world_height 	= Clickgraph.get_world_height();
		int margin_top 		= this.margins[MarginSpec.TOP.getValue()];
		int margin_bottom 	= this.margins[MarginSpec.BOTTOM.getValue()];
		int margin_left		= this.margins[MarginSpec.LEFT.getValue()];
		int margin_right	= this.margins[MarginSpec.RIGHT.getValue()];
		
		Clickgraph.debug("world is " + world_width + " wide x " + world_height + " high - with padding " + this.padding);
		Clickgraph.debug("**** VT margins TOP: " + margin_top + " and BOTTOM: " + margin_bottom);
		Clickgraph.debug("**** HZ margins LEFT: " + margin_left + " and RIGHT: " + margin_right);

		// given the number of rows and columns in the workspace, the margin and padding information,
		// compute height and width of the remaining area and what can be allocated to each graph window
		//	 top-to-bottom layout:  top_margin <padding> [window] <padding> [window] ... <padding> bottom_margin
		//   left-to-right layout:  left_margin <padding> [window] <padding> [window] ... <padding> right_margin
		// 
		//                total available       2 margins              number of windows * 2 of padding  
		double gr_height = (world_height - margin_top - margin_bottom - (int)(this.padding * (this.rows * 2))) / this.rows;
		double gr_width = (world_width - margin_left - margin_right - (int)(this.padding * (this.cols * 2))) / this.cols;
		
		// our windows need to be square, so take the minimum available size from horizontal and vertical
		int graph_size = Math.min((int)gr_width, (int)gr_height);
		Clickgraph.debug("final graph size = " + graph_size + " x " + graph_size + " (does not include padding)");
	
		// reducing the horz and vert graph size to a square will potentially leave some residual space horizontally or vertically
		// this space gets split evenly between the two sides (top/bottom or left/right)
		double remaining_rows = world_height - (margin_top + margin_bottom + (int)(this.rows * (graph_size + (this.padding * 2))));
		double remaining_cols = world_width - (margin_left + margin_right + (int)(this.cols * (graph_size + (this.padding * 2))));
	
		Clickgraph.debug("extra space on top/bottom = " + remaining_rows / 2);
		Clickgraph.debug("extra space on left/right = " + remaining_cols / 2);
	
		// Graph #1's upper left corner is located at [g_xpos, g_ypos] 
		//   as we go through the loop below, we layout row x col graphers, advancing g_xpos and g_ypos by graph_size + 2 padding
		//   graphs are layed out in row-major order (gnum)
		gnum = 1;
		
		//  in general graphers will be named <workspace>1, <workspace>2... but if ws is 1x1 don't use number
		if (this.rows == 1 && this.cols == 1) {
			numberingp = false;
			Clickgraph.debug("This workspace is 1x1 - turning off grapher numbering");
		}
		
		
		//  Note: in the NetLogo world, the Y dimension decreases moving down (so we subtract size each row)
		//             upper left corner of the world   extra             padding     margins
		double g_ypos = Clickgraph.get_max_pycor() - (remaining_rows / 2) - (int) this.padding - margin_top;	
		double g_xpos = Clickgraph.get_min_pxcor() + (remaining_cols / 2) + (int) this.padding + margin_left;	
	
		for (r=0; r<this.rows; r++) {
			for (c=0; c<this.cols; c++) {
				// graphs inherit their names from their parent workspace name + their graph number using row major ordering
				// i.e.  wsname1, wsname2, wsname3   for a 2 x 3 workspace
				//		 wsname4, wsname5, wsname6
				
				gname = numberingp ? String.format("%s%d", this.name, gnum) : String.format("%s", this.name);
				Clickgraph.debug("updateGraphers: creating grapher with name '" + gname + "'");
				
				if ((g = this.findGrapherByName(gname)) == null) {
					// we create square graphers - but they need not be
					this.graphers.add(new Grapher(gname, g_xpos, g_ypos, graph_size, graph_size));
				} else {
					g.update_pos_size(g_xpos, g_ypos, graph_size, graph_size);
				}
				
				g_xpos += graph_size + (int) (this.padding * 2);	// move right by one graph window + 2 x padding 
				gnum += 1;
			}
		
			// reset our X coordinate and advance Y by one row
			g_xpos = Clickgraph.get_min_pxcor() + (remaining_cols / 2) + (int) this.padding + margin_left;
			g_ypos -= (graph_size + (this.padding * 2));
		}
	}
	
	/* updateMargins: given margin-specifier (top, vertical, all, ...) and size
		update the margin_top, _bottom, _left and _right instance variables in workspace */
		
	private void updateMargins(String margin_spec, int size) {
	  
		int [] margin_idx_vec = Workspace.mapMarginSpec(margin_spec);
		Clickgraph.debug("margin_spec: " + margin_spec + " => array of length: " + margin_idx_vec.length );
		
		for(int i = 0; i < margin_idx_vec.length; i++) {
		    this.margins[margin_idx_vec[i]] = size;
			Clickgraph.debug(" update margins[" + margin_idx_vec[i] + "] = " + size );
		}
	}
	
	
	/* redraw: draw a complete workspace */
	private void redraw(Context context) {
		// get the drawing area from the Netlogo Context
		BufferedImage drawing = context.getDrawing();
		Graphics2D gc = drawing.createGraphics();		// graphics context
		
		gc.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
		
		Clickgraph.eraseWorld(context);

		// for now, draw the workspace name in the upper right corner of the workspace
		// *** not currently used by Graphing in Groups  11-26-14
		// String wsname = new String("Workspace: " + this.name);
		// gc.drawString(wsname, Clickgraph.convertXCoord(Clickgraph.get_min_pxcor() + 10), Clickgraph.convertYCoord(Clickgraph.get_max_pycor() - 10));	
		
		for (Grapher g : this.graphers) {
			g.redraw(context);		// pass the workspace so that graphers can access parent vars
		}
	}
	
	
	/* dump: output basic workspace information to stdout */
	void dump () {
		Clickgraph.debug("---- Workspace ----");
		Clickgraph.debug(this.name + " is " + this.rows + " rows x " + this.cols + " cols");
		for (Grapher g : this.graphers) {
			g.dump();
		}
	}
	
	
	/********************************************************************************
	 *  CLASS methods
	 *******************************************************************************/
	
	static public void initWorkspaces() {
		Workspace.WorkspaceList.clear();	// clear the class list of workspaces
		Grapher.initGraphers();				// make sure to clear out any subordinate graphers
	}
	
	static public void Regraph(Context context) {
		for (Workspace w: WorkspaceList) {
			if (w.visiblep) {
				w.redraw(context);
			}
		}
	}
	
	static Workspace findWorkspaceByName(String name) {
		int i;
		Workspace g;
		for (i = 0; i < Workspace.WorkspaceList.size(); i++) {
	      	g = Workspace.WorkspaceList.get(i);
			if (name.equals(g.name)) {
				return g;
			}
	    }
	
		Clickgraph.debug("findWorkspaceByName: did not find workspace '" + name + "' - (this is correct at create time)");
	    return null;
	}
	
	static int[] mapMarginSpec(String margin_spec) {
		int[] rval;
		
		MarginSpec mspec = MarginSpec.valueOf(margin_spec.toUpperCase());
     
		switch (mspec) {
			case ALL: 
				rval = new int[] {MarginSpec.TOP.getValue(), MarginSpec.BOTTOM.getValue(), MarginSpec.LEFT.getValue(), MarginSpec.RIGHT.getValue()};
				break;
			case TOP: 
				rval = new int[] {MarginSpec.TOP.getValue()};
				break;
			case BOTTOM:
				rval = new int[] {MarginSpec.BOTTOM.getValue()};
				break;
			case LEFT: 
				rval = new int[] {MarginSpec.LEFT.getValue()};
				break;
			case RIGHT:
				rval = new int[] {MarginSpec.RIGHT.getValue()};
				break;
			case VERTICAL: 
				rval = new int[] {MarginSpec.TOP.getValue(), MarginSpec.BOTTOM.getValue()};
				break;
			case HORIZONTAL:
				rval = new int[] {MarginSpec.LEFT.getValue(), MarginSpec.RIGHT.getValue()};
				break;
			default:
				rval = new int[] {};
				break;
		}
		return rval;
	}

}
